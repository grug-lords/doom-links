# Endpoint list:


## Login

### Login Endpoint
This endpoint handles user login functionality.

### Endpoint URL
| POST |  /token |

### Request Parameters
| Parameter | Type | Description |
|-----------|----------|------------------------|
| email | string | User's email address |
| password | string | User's password |

### Responses
| Status Code | Description |
|-------------|--------------------------------------|
| 200 | Successful login, user redirected |
| 400 | Incorrect email or password provided |


## Logout

### Logout Endpoint
This endpoint handles user logout functionality.

### Endpoint URL
| DELETE |  /token |

### Responses
| Status Code | Description |
|-------------|---------------------------------------|
| 302 | User successfully logged out, redirected to login page |

### Example Usage
To log out, simply access the `/logout` endpoint. This can be done by clicking a logout button or by navigating to the logout URL directly. After logging out, the user will be automatically redirected to the login page.


## Create Account

### Description
This endpoint handles creating a user account.  When the user submits data successfully, the account will be created.

### Endpoint URL
| POST | /accounts |

### Request Parameters
none

### Responses
| Status Code | Description |
|-------------|--------------------------------------|
| 200 | Successfully created user profile |
| 400 | could not create user profile |

### Example Usage
```
{
	“username”: “user1”,
    "email": "user1@aol.com",
	“password”: “password”,
	"password_confirmation": "string"
}
```

## Update Profile Info

### Description
This endpoint handles updating a user profile.  When the user submits data successfully, the profile will be updated.

### Endpoint URL
| PUT | /users/self |

### Request Parameters
| Parameter | Type | Description |
|-----------|----------|------------------------|
| token     | auth     | token of currently logged-in user |

### Responses
| Status Code | Description |
|-------------|--------------------------------------|
| 200 | Successfully updated profile |
| 401 | Must be logged in to update profile |

### Example Usage
```
{
	“location”: “seattle”,
	“reachme”: “insta: @reach_me, 555-555-5555”
}
```

## Update Avatar

### Description
This endpoint handles updating a user profile with a new avatar image.

### Endpoint URL
| PUT | /users/self/profile |

### Request Parameters
| Parameter | Type | Description |
|-----------|----------|------------------------|
| token     | auth     | token of currently logged-in user |

### Responses
| Status Code | Description |
|-------------|--------------------------------------|
| 200 | Successfully updated profile |
| 401 | Must be logged in to update account |

### Example Usage
```
{
	“avatar_pic”: appa.png
}
```

## Delete Profile

### Description
This endpoint will delete a user profile and all its information from the databases

### Endpoint URL
| DELETE | /users/:id |

### Request Parameters
| Parameter | Type | Description |
|-----------|----------|------------------------|
| user ID | int | DB id for the profile to be deleted |

### Responses
| Status Code | Description |
|-------------|--------------------------------------|
| 200 | Deleted user |
| 400 | User does not exist |

### Example Usage
JSON Response:
```
{
	deleted: true
}
```


## View profile

### Description
This api endpoint will retrieve a user’s profile information.

### Endpoint URL
 | GET | /users/:username |

### Request Parameters
| Parameter | Type | Description |
|-----------|----------|------------------------|
| username | str | username for the profile to be displayed |

### Responses
| Status Code | Description |
|-------------|--------------------------------------|
| 200 | Retrieved User Profile |
| 404 | User not found |
| 401 | Must be logged in to view profile |

### Example Usage
JSON Response:
```
{
	“username”: “user1”,
    "email": "user1@example.com",
    “location”: “seattle”,
	“avatar”: “https://i.pinimg.com/474x/1a/1d/88/1a1d88a880db8670c804e130d7be20d0.jpg”,
	“reachme”: “insta: @reach_me, 555-555-5555”,
	“collection”: “collection_id”,
}
```

## View own profile

### Description
This api endpoint will retrieve the logged-in user’s profile information.

### Endpoint URL
 | GET | /users/self |

### Request Parameters
| Parameter | Type | Description |
|-----------|----------|------------------------|
| token | auth | token for the currently logged-in user |

### Responses
| Status Code | Description |
|-------------|--------------------------------------|
| 200 | Retrieved User Profile |
| 401 | Must be logged in to view profile |

### Example Usage
JSON Response:
```
{
	“username”: “user1”,
    "email": "user1@example.com",
    “location”: “seattle”,
	“avatar”: “https://i.pinimg.com/474x/1a/1d/88/1a1d88a880db8670c804e130d7be20d0.jpg”,
	“reachme”: “insta: @reach_me, 555-555-5555”,
	“collection”: “collection_id”,
}
```


## View Collection

### Description
Executing this api will retrieve all cards in the selected profile’s collection and display them as JSON information.

### Endpoint URL
| GET | /doomlings/collection/:user_id |

### Request Parameters
| Parameter | Type | Description |
|----------------|--------|-------------------------|
| user ID | int | DB id of user whose collection will be displayed |

### Responses
| Status Code | Description |
|-------------|--------------------------------------|
| 200 | Successfully got collection |
| 400 | cannot display collection |

Example Output:
```
[
    {
        "collection": "Classic",
        "cardIdNumber": "1",
        "subCollection": null,
        "name": "Echolocation",
        "color": "Blue",
        "rarity": "0-Bundle",
        "cardType": "Trait",
        "subtype": "Dominant, Persistent",
        "points": "4",
        "imageUrl": "https://global-uploads.webflow.com/61494314e1322b4a29b5462a/626aedcf81ca4dff07fa6ade_6171ef3214b0bb5a253b11af_Echolocation.png",
        “fortrade”: “yes”,
        “holo”: “no”,
    },
    {
        "collection": "Classic",
        "cardIdNumber": "1",
        "subCollection": null,
        "name": "Echolocation",
        "color": "Blue",
        "rarity": "0-Bundle",
        "cardType": "Trait",
        "subtype": "Dominant, Persistent",
        "points": "4",
        "imageUrl": "https://global-uploads.webflow.com/61494314e1322b4a29b5462a/626aedcf81ca4dff07fa6ade_6171ef3214b0bb5a253b11af_Echolocation.png",
        “fortrade”: “no”,
        “holo”: “no”,
    },
    {
        "collection": "Classic",
        "cardIdNumber": "2",
        "subCollection": null,
        "name": "Immunity",
        "color": "Blue",
        "rarity": "0-Bundle",
        "cardType": "Trait",
        "subtype": "Dominant, Drop of Life",
        "points": "4",
        "imageUrl": "https://global-uploads.webflow.com/61494314e1322b4a29b5462a/626aee011c08e1ff03c7316c_6171f0177614bcb729703d8e_Immunity.png",
        “fortrade”: “no”,
        “holo”: “no”,
    }
…
]
```


## Update Collection
Add Card to Collection

### Description
Executing this api will update the selected profile’s collection to add a card.

### Endpoint URL
| POST | /doomlings/collection/ |

### Request Parameters
| Parameter | Type | Description |
|----------------|--------|-------------------------|
| token | auth | token of the currently logged-in user |

### Responses
| Status Code | Description |
|-------------|--------------------------------------|
| 200 | Successfully added card to collection |

Example Output:
```
[
    {
        "collection": "Classic",
        "cardIdNumber": "1",
        "subCollection": null,
        "name": "Echolocation",
        "color": "Blue",
        "rarity": "0-Bundle",
        "cardType": "Trait",
        "subtype": "Dominant, Persistent",
        "points": "4",
        "imageUrl": "https://global-uploads.webflow.com/61494314e1322b4a29b5462a/626aedcf81ca4dff07fa6ade_6171ef3214b0bb5a253b11af_Echolocation.png",
        “fortrade”: “yes”,
        “holo”: “no”,
    },
    …
]
```


## View a Card

### Description
Get detailed information for a single card

### Endpoint URL
| GET | /doomlings/catalog/:card_id |

### Request Parameters
| Parameter | Type | Description |
|----------------|--------|-------------------------|
| card_id | int | Displays details of the card |


### Responses
| Status Code | Description |
|------------------|-------------------------|
| 200 | Successfully got card |
| 404 | Card not found |

### Example Output
```
{
    "collection": "Classic",
    "cardIdNumber": "1",
    "subCollection": null,
    "name": "Echolocation",
    "color": "Blue",
    "rarity": "0-Bundle",
    "cardType": "Trait",
    "subtype": "Dominant, Persistent",
    "points": "4",
    "imageUrl": "https://global-uploads.webflow.com/61494314e1322b4a29b5462a/626aedcf81ca4dff07fa6ade_6171ef3214b0bb5a253b11af_Echolocation.png",
}
```


## View all Cards

### Description
Get a list of all cards in the catalog

### Endpoint URL
| GET | /doomlings/catalog/ |

### Request Parameters
none

### Responses
| Status Code | Description |
|------------------|-------------------------|
| 200 | Successfully got cards |

### Example Output
```
[
    {
        "collection": "Classic",
        "cardIdNumber": "1",
        "subCollection": null,
        "name": "Echolocation",
        "color": "Blue",
        "rarity": "0-Bundle",
        "cardType": "Trait",
        "subtype": "Dominant, Persistent",
        "points": "4",
        "imageUrl": "https://global-uploads.webflow.com/61494314e1322b4a29b5462a/626aedcf81ca4dff07fa6ade_6171ef3214b0bb5a253b11af_Echolocation.png",
        “fortrade”: “yes”,
        “holo”: “no”,
    },
    {
        "collection": "Classic",
        "cardIdNumber": "1",
        "subCollection": null,
        "name": "Echolocation",
        "color": "Blue",
        "rarity": "0-Bundle",
        "cardType": "Trait",
        "subtype": "Dominant, Persistent",
        "points": "4",
        "imageUrl": "https://global-uploads.webflow.com/61494314e1322b4a29b5462a/626aedcf81ca4dff07fa6ade_6171ef3214b0bb5a253b11af_Echolocation.png",
        “fortrade”: “no”,
        “holo”: “no”,
    },
    {
        "collection": "Classic",
        "cardIdNumber": "2",
        "subCollection": null,
        "name": "Immunity",
        "color": "Blue",
        "rarity": "0-Bundle",
        "cardType": "Trait",
        "subtype": "Dominant, Drop of Life",
        "points": "4",
        "imageUrl": "https://global-uploads.webflow.com/61494314e1322b4a29b5462a/626aee011c08e1ff03c7316c_6171f0177614bcb729703d8e_Immunity.png",
        “fortrade”: “no”,
        “holo”: “no”,
    }
…
]
```


## View all forum posts

### Description
This api endpoint retrieves all active posts for trades that people want to complete.

### Endpoint URL
GET | /posts |

### Request Parameters
none

### Responses
| Status Code | Description |
|-------------|--------------------------------------|
| 200 | Successfully got all posts |

### Example Usage
JSON Response:
```
{
	“posts”: [
		{
			“id”: 1,
			“user”: 35,
			“trading”: [5, 32, 276],
			“wants”: [1, 28],
			"title": "string",
            "body": "string",
            "status": "string",
            "created": "2024-02-08T01:17:39.213Z",
            "username": "user1",
            "contact_info": "discord: @discord_handle",
            "avatar_pic": "http://app/static/images/avatar.png"
        },
        {
			“id”: 2,
			“user”: 3,
			“trading”: [1, 32, 76],
			“wants”: [276, 34, 103],
			"title": "string",
            "body": "string",
            "status": "string",
            "created": "2024-02-08T01:17:39.213Z",
            "username": "user2",
            "contact_info": "signal: 555-555-5555",
            "avatar_pic": "http://app/static/images/avatar2.png"
        },
        …
    ]
}
```


## View single forum post

### Description
This api endpoint retrieves a singular active post for trade and provides all of its details.

### Endpoint URL
| GET | /posts/:post_id |

### Request Parameters
| Parameter | Type | Description |
|-----------|----------|------------------------|
| post ID | int | DB id of post to be displayed |

### Responses
| Status Code | Description |
|-------------|--------------------------------------|
| 200 | Successfully got post |
| 400 | Post does not exist |

### Example Usage
JSON Response:
```
{
    “id”: 1,
    “user”: 35,
    “trading”: [5, 32, 276],
    “wants”: [1, 28],
    "title": "string",
    "body": "string",
    "status": "string",
    "created": "2024-02-08T01:17:39.213Z",
    "username": "user1",
    "contact_info": "discord: @discord_handle",
    "avatar_pic": "http://app/static/images/avatar.png"
}
```


## Create forum post

### Description
This api endpoint will create a forum post for a card you want to trade.

### Endpoint URL
| POST | /posts/ |

### Request Parameters :D
none

### Responses
| Status Code | Description |
|-------------|--------------------------------------|
| 200 | Successfully created post |
| 400 | Failed to make post |

### Example Usage
JSON Response:
{
	“id”: 1,
    “user”: 35,
    “trading”: [5, 32, 276],
    “wants”: [1, 28],
    "title": "string",
    "body": "string",
    "status": "string",
    "created": "2024-02-08T01:17:39.213Z",
    "username": "user1",
    "contact_info": "discord: @discord_handle",
    "avatar_pic": "http://app/static/images/avatar.png"
}


## Update forum post

### Description
This api endpoint will update a forum post for a card you want to trade.

### Endpoint URL
| PUT | /posts/:post_id |

### Request Parameters
| Parameter | Type | Description |
|-----------|----------|------------------------|
| post ID | int | DB id for the post to be updated |

### Request Shape (json body)
```
{
    “trading”: [5, 32, 276],
    “wants”: [1, 28],
    "title": "string",
    "body": "string",
}
```

### Responses
| Status Code | Description |
|-------------|--------------------------------------|
| 200 | Successfully updated post |
| 400 | Failed to update post |

### Example Usage
JSON Response:
```
{
	“id”: 1,
    “user”: 35,
    “trading”: [5, 32, 276],
    “wants”: [1, 28],
    "title": "string",
    "body": "string",
    "status": "string",
    "created": "2024-02-08T01:17:39.213Z",
    "username": "user1",
    "contact_info": "discord: @discord_handle",
    "avatar_pic": "http://app/static/images/avatar.png"
}
```


## Delete forum post

### Description
This api endpoint will delete a forum post.

### Endpoint URL
| DELETE | /posts/:post_id/ |

### Request Parameters
| Parameter | Type |
|-----------|----------|
| post ID | int | DB id of post to be deleted |

### Responses
| Status Code | Description |
|-------------|--------------------------------------|
| 200 | Successfully deleted post |
| 400 | Failed to delete post |

### Example Usage
JSON Response:
```
{
	Deleted: true
}
```
