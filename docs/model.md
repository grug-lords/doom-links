# Data models

## Users

| Name         | Type    | Unique | Optional |
| ------------ | ------- | ------ | -------- |
| user_id      | int     | yes    | no       |
| username     | varchar | yes    | no       |
| email        | varchar | yes    | no       |
| password     | varchar | no     | no       |
| location     | varchar | no     | yes      |
| contact_info | varchar | no     | yes      |
| is_admin     | bool    | no     | no       |
| avatar_pic   | varchar | no     | yes      |

The user table contains information about a user for their profile.

## Cards

| Name           | Type    | Unique | Optional |
| -------------- | ------- | ------ | -------- |
| id             | float   | yes    | no       |
| name           | varchar | no     | no       |
| color          | varchar | no     | no       |
| type           | varchar | no     | no       |
| sub_type       | varchar | no     | yes      |
| points         | varchar | no     | yes      |
| collection     | varchar | no     | no       |
| sub_collection | varchar | no     | yes      |
| rarity         | varchar | no     | no       |
| image          | varchar | no     | no       |

The cards table contains information about every card in the catalog, and is prepopulated.

## Posts

| Name           | Type                     | Unique | Optional |
| -------------- | ------------------------ | ------ | -------- |
| id             | int                      | yes    | no       |
| user_id        | reference to user entity | no     | no       |
| trading        | int                      | no     | no       |
| wants          | int                      | no     | no       |
| title          | varchar                  | no     | no       |
| body           | text                     | no     | no       |
| status         | varchar                  | no     | no       |
| created        | timestamp w timezone     | no     | no       |

The posts table contains information about every post in the forum.

## Collections

| Name           | Type                     | Unique | Optional |
| -------------- | ------------------------ | ------ | -------- |
| id             | int                      | yes    | no       |
| user_id        | reference to user entity | no     | no       |
| card_id        | reference to card entity | no     | no       |
| trading        | bool                     | no     | no       |
| amount         | int                      | no     | no       |
| holos          | int                      | no     | no       |

The collections table contains information about each user's collection of cards.
