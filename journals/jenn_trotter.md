### week 4:
2/1 - today was for the update profile view frontend function.  i spent the morning reviewing and approving merge requests, and then got into dealing with avatar images.  i figured out how to upload the file to react, turn that into a base64 bytestring, send that to the backend to be stored in the users table as a bytearray, and then pull from that for the profile view… but i kept running into an error when trying to view the profile, saying that the headers were too large.  i spent a while trying to figure out how to encode/decode or compress the image file, before alex recommended asking paul.  Paul recommended an entirely different approach, where i use a fastAPI feature (still need to figure out what he meant here) to store the images on the server in /static/images and then in the DB just keep a filepath to where the images are stored.  i think this means totally rewriting the update profile endpoint, query, router, etc… so i’m very discouraged but i’ll work on it!

1/31 - today when getting started on adding avatar images, i typo-ed my login info and still got redirected to an empty profile page.  This made me realize that the auth package we were given doesn’t fail or return any errors on bad data.  After talking it through with Alex, I learned there were three options for dealing with this: 1- pass the list of all users into this function and check data entered against the database [this would mean doing more fetches at the same time rather than passing the work to the client], 2- using useRef to check the status of the token in the middle of the login function [this seems like high wizardry to me and I don’t fully understand it], or 3- rewriting the login function to happen in my code instead of in the JWTdown package, just cherry-picking the pieces needed like getToken and setToken to make it work.  I spent most of the day on that, and finally got it so bad data on login returns errors/alerts and doesn’t redirect you (redirect is based on token).  After that I got started on letting users upload avatar images.  I’ll need to figure out how to turn an uploaded image file to base-64 bytestring, then send that to the database as a bytearray, pull from database as a bytearray, convert back into a base-64 bytestring, and then display that as an image.

1/30 - today i realized that i couldn’t use my UsernameOut type because i needed the full profile data to display in profile view. Changed it to a UserProfileOut and squidged the data to make it work.  then i worked on the Create Account form - the hardest part was figuring out how to get the custom 400 error messages to display.  To-dos: update profile form, figure out how to pull/store images as BYTEA, and CSS prettification for all user pages and forms.

1/29 - solo again.  today I worked on creating the profile view and getting the site to redirect to profile view after login. I wrote a custom users/self view to bypass the problems with string interpolation in useNavigate, and made a custom UsernameOut response type to pull just the username from the token.

### week 3:
1/26 - I'm solo on accounts pages today. I spent a long time trying to figure out how to get the profile view to pull the right data based on username, and then how to get the pathing to actually work.  Alex's help understanding how data gets passed around was invaluable.  To-do: populate the profile view with actual user data; continue on sorting out how to get the "my profile" path to work properly (string interpolation in useNavigate is a no-go.)

1/25 - today we got the logout function to work and worked on getting login/logout etc. to be token-dependent in the navbar.

1/24 - today we started on frontend pages and got the login form to work.  This meant getting Alex to explain how the login/logout features of JWTdown work; we couldn't find it in the documentation.

1/23 - continue on user accounts functions. Mitch and I wrote the "update" and "delete" profile functions and created a new view_account query to pull the data we needed. To-do: create the front-end pages for these functions, remembering that we'll have to encode/de-encode the BYTEA type at the front-end.

1/22 - continue on user accounts functions. Mitch and I reorganized the users table to include optional fields and remove full_name, which we don't need. It now includes a BYTEA field for avatar, which will have to be serialized and deserialized in the REACT code so that it will fit in the database. We also worked on the update account function, and spent a lot of time understanding how data gets passed between the database and the functions. To-do: test the update account function, add the delete account function, and create front-end pages for user accounts and functions.

### week 2:
1/19 - discuss value of using google api for login; decided against. Research signing keys and authentication to work on user account issues with Mitch.  We finished create accounts, login/logout, authentication, and get all users endpoints, and created the users table.

1/18 - start work on accounts issues; research google and discord login API integrations

1/17 - continue drafting issues, consider how cards interact with collection and forum posts

1/16 - continue work on github issues, working on structure for forum post and collection interaction.

### week 1:
1/12 - main work on github issues, finalized wireframes and endpoints, first merge for journal.

1/11 - main work on API endpoints, submitted MVP, started on github issues.

1/10 - main work on wireframes, started work on API endpoints, got containers up/running.

1/9 - chose team name, started work on wireframes, appreciated Devin's work on repo and JSON.

1/8 - chose project idea with team, discussed shape of site, determined some stretch goals.
