day 1 - Collaborated with team to idealize a website, began work on json file for database of all cards
day 2 - Became the lord of the grugs with fellow grugs, created repo and continued work on the json file, started the wireframe of the project
day 3 - Began work on the API endpoints, created the docker-compose and database with volumes
day 4 - Finished json file and API endpoints
day 5 - Finished wireframe and began to push to main and begin coding

day 6 - Created table for cards and established connection to the database through beekeeper
day 7 - Fixed Table for cards and added all their dta to be inserted upon migrations
day 8 - Created API endpoint to get all cards from the backend
day 9 - 100% done with the functionality of the frontend view for the catalog and finishing up the styling of the page

day 10 - Began to add a filter feature with smart grug friend to the catalog
day 11 - Finished filter feature with smart grug
day 12 - Created an endpoint to get a single cards description
day 13 - Made a front end page to display a single cards description
day 14 - Stylized description page for a single card

day 15 - Reviewed merge requests and took an overview look at the project
day 16 - Began to diagram the collections database with fellow grugs
day 17 - Created collections table and researched joins
day 18 - Much SQL practice

day 19 - Finished unit tests and began to make queries and routers for collections

