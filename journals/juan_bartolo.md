#### Juan Bartolo's Super Cool Journal

## 1/8 - 1/12

This week was spent majority planning. We talked as a team about the wireframe, MVP, and API endpoints design.
We did the wireframe on Excalidraw and the rest was done on google docs.

## 1/15 - 1/19

This week I spent learning more about PostgreSQL and learning how to use it.First I created my posts table and referenced them to the users table. After that I created 2 enpoints, a GET /post/and a POST /post/ endpoint. The get post will just use SQL to Select every single posts instance and then return them as data. The POST endpoint will take in information and add more information from the backend to then create a new post based on a user ID.

## 1/22 - 1/26
First I created a update a post feature and a delete a post endpoints. After that a lot of the week was spent working with Devin
on creating a working card filter for the catalog. We got it working and did it through switching the drive on the branch.
## 1/29 - 1/2
This week was spent creating a working create a post front end. I needed To have checkmarks linked to every cards and also figure out a way for those checkmarks to stick even after  user searches for the cards they want to trade or that they want. Then I started my forum Post and tried getting a carousel working but gave up after breaking my branch so I opted for making a simple multiple page react page.
## 2/5 - 2/9
I cleaned up the forum front page and spent time getting a grid for all the cards you want to trade and want. I upgraded my post get_all queries to have a Inner Join with the users table so I can get all the information I need for a post. Then I completed a unit test for the get_all.
