# DoomLinks
## A Collection Tracker and Trading Forum for Doomlings

- Devin Lacey
- Jenn Trotter
- Juan Bartolo
- Mitchell Mora

## Design

- [Project wireframes](docs/wireframes.md)
- [API documentation](docs/endpoints.md)
- [Data model](docs/model.md)

## Intended Market and Usage

This site is designed to provide a way for players of the game
Doomlings to track and manage their card collections, and to
facilitate trading cards between players.  Users of the site can
view the filterable catalog of all cards and the forum of all
posted trade offers/desires.  In order to manage their collection
by adding or removing cards, users will need to create an account
and log in.

## Project Initialization

To make use of this application on your local machine, please
follow these steps:

1. Clone the repository to your local machine
2. CD into the new project directory
3. Run `docker volume create card-link`
4. Run `docker compose build`
5. Run `docker compose up`
6. Exit the container CLI, navigate to
http://grug-lords.gitlab.io/doom-links/
and enjoy!
