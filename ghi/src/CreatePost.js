/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { MagnifyingGlassIcon } from "@heroicons/react/24/solid";
import { useNavigate } from "react-router-dom";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";

import PostCard from "./components/PostCard";
import MissingCards from "./components/MissingCards";

const CreatePost = ({ cards }) => {
  const { token } = useAuthContext();
  const navigate = useNavigate();
  const [search, setSearch] = useState("");
  const [searchWants, setSearchWants] = useState("");
  const [formData, setFormData] = useState({
    title: "",
    body: "",
  });
  const [trading, setTrading] = useState(new Set([]));
  const [wants, setWants] = useState(new Set([]));
  const [tradingTrue, setTradingTrue] = useState(false);
  const [wantsTrue, setWantsTrue] = useState(false);
  const [collectionCards, setCollection] = useState([]);
  const [allCards, setAllCards] = useState(cards);

  const handleTradingChange = (e) => {
    const value = parseFloat(e.target.value);
    const arr = trading;
    if (arr.has(value)) {
      arr.delete(value);
      setCollection((current) => {
        const index = current.map((card) => card.card_id).indexOf(value);
        let currentClone = [...current];
        currentClone[index].checked = false;
        return currentClone;
      });
    } else {
      setCollection((current) => {
        const index = current.map((card) => card.card_id).indexOf(value);
        let currentClone = [...current];
        currentClone[index].checked = true;
        return currentClone;
      });
      arr.add(value);
    }
    setTrading(arr);
  };
  const handleWantsChange = (e) => {
    const value = parseFloat(e.target.value);
    const arr = wants;
    if (arr.has(value)) {
      arr.delete(value);
      setAllCards((current) => {
        const index = current.map((card) => card.card_id).indexOf(value);
        let currentClone = [...current];
        currentClone[index].checked = false;
        return currentClone;
      });
    } else {
      arr.add(value);
      setAllCards((current) => {
        const index = current.map((card) => card.card_id).indexOf(value);
        let currentClone = [...current];
        currentClone[index].checked = true;
        return currentClone;
      });
    }
    setWants(arr);
  };

  const handleFormChange = (e) => {
    const inputName = e.target.name;
    const value = e.target.value;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };
  const handleSearchChange = (e) => {
    const value = e.target.value;
    setSearch(value);
  };
  const handleSearchWantsChange = (e) => {
    const value = e.target.value;
    setSearchWants(value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (trading.size !== 0 && wants.size !== 0) {
      const accountUrl = `${process.env.REACT_APP_API_HOST}/token`;
      const config = {
        method: "GET",
        credentials: "include",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
      const responseAccount = await fetch(accountUrl, config);
      const account = await responseAccount.json();

      const url = `${process.env.REACT_APP_API_HOST}/posts`;

      const data = {
        user_id: parseInt(account.account.user_id),
        trading: [...trading],
        wants: [...wants],
        title: formData.title,
        body: formData.body,
      };
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      };
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setFormData({
          title: "",
          body: "",
        });
        setTrading(new Set([]));
        setWants(new Set([]));
        setSearch("");
        setSearchWants("");
        navigate("/doomlings/posts");
      }
    } else {
      if (trading.size === 0) {
        setTradingTrue(true);
      } else {
        setTradingTrue(false);
      }
      if (wants.size === 0) {
        setWantsTrue(true);
      } else {
        setWantsTrue(false);
      }
      alert("Missing cards to trade or cards that you want");
    }
  };

  const userCollection = async () => {
    if (token) {
      const config = {
        method: "GET",
        credentials: "include",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
      const response = await fetch(
        `${process.env.REACT_APP_API_HOST}/doomlings/collection`,
        config
      );
      const data = await response.json();
      let newArr = data.map((card) => {
        card["checked"] = false;
        return card;
      });
      setCollection(newArr);
    }
  };
  const AllCards = () => {
    if (cards) {
      let deepArr = [];
      for (let card of cards) {
        card["checked"] = false;
        deepArr.push(card);
      }
      setAllCards(deepArr);
    }
  };

  useEffect(() => {
    userCollection();
  }, [token]);
  useEffect(() => {
    AllCards();
  }, [cards]);

  return (
    <div className="flex justify-center space-x-2 py-7 px-3">
      <form onSubmit={handleSubmit} className="space-y-10">
        <div className="space-y-12">
          <div className="border-b border-gray-900/10 pb-12">
            <h2 className="text-base font-semibold leading-7 text-white">
              New Post
            </h2>
            <p className="mt-1 text-sm leading-6 text-gray-300">
              Create a new post to trade your cards.
            </p>

            <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
              <div className="sm:col-span-4">
                <label
                  htmlFor="title"
                  className="block text-sm font-medium leading-6 text-white"
                >
                  Title
                </label>
                <div className="mt-2">
                  <div className="text-gray-300 bg-gray-700 flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                    <input
                      onChange={handleFormChange}
                      value={formData.title}
                      type="text"
                      name="title"
                      id="title"
                      required
                      className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-white placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                      placeholder="Trading for ..."
                    />
                  </div>
                </div>
              </div>

              <div className="col-span-full">
                <label
                  htmlFor="body"
                  className="block text-sm font-medium leading-6 text-white"
                >
                  Description
                </label>
                <div className="mt-2">
                  <textarea
                    onChange={handleFormChange}
                    value={formData.body}
                    id="body"
                    name="body"
                    rows={4}
                    required
                    className="block px-5 w-full bg-gray-700 rounded-md border-0 py-1.5 text-white shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                  />
                </div>
                <p className="mt-3 text-sm leading-6 text-gray-300">
                  Write a few sentences about your cards and the ones you're
                  looking for.
                </p>
              </div>
            </div>
          </div>

          <div className="border-b border-gray-900/10 pb-12">
            <h2 className="text-base font-semibold leading-7 text-white">
              Cards you are trading
            </h2>
            <p className="mt-1 text-sm leading-6 text-gray-300">
              Only cards in your collection will pop up
            </p>
            {tradingTrue ? (
              <MissingCards message={"Select cards for trade"} />
            ) : null}
            <div className="flex flex-1 items-center justify-center px-2 lg:ml-6 mobile:justify-end sm:justify-end md:justify-end lg:justify-end xl:justify-end 2xl:justify-end">
              <div className="w-full max-w-lg lg:max-w-xs md:max-w-64 sm:max-w-60 mobile:max-w-52">
                <label htmlFor="searchTrading" className="sr-only">
                  Search
                </label>
                <div className="relative">
                  <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                    <MagnifyingGlassIcon
                      className="h-5 w-5 text-gray-400"
                      aria-hidden="true"
                    />
                  </div>
                  <input
                    onChange={handleSearchChange}
                    value={search}
                    id="searchTrading"
                    name="searchTrading"
                    className="block w-full rounded-md border-0 bg-gray-700 py-1.5 pl-10 pr-3 text-white ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    placeholder="Search"
                    type="search"
                  />
                </div>
              </div>
            </div>
            <div className="flex justify-center py-4">
              <div className="sm:col-span-3">
                <fieldset
                  onChange={handleTradingChange}
                  value={trading}
                  required
                  name="trading"
                  id="trading"
                >
                  <legend key="legend-2" className="sr-only">
                    Cards in your collection
                  </legend>
                  <div className="grid mobile:grid-cols-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-4 2xl:grid-cols-4 overflow-y-auto h-96">
                    {collectionCards
                      .filter((card) => {
                        if (
                          search !== "" &&
                          card.name.toUpperCase().includes(search.toUpperCase())
                        ) {
                          return card;
                        } else if (search !== "") {
                          return null;
                        } else {
                          return card;
                        }
                      })
                      .map((card) => {
                        return <PostCard card={card} checked={card.checked} />;
                      })}
                  </div>
                </fieldset>
              </div>
            </div>
          </div>

          <div className="border-b border-gray-900/10 pb-12">
            <h2 className="text-base font-semibold leading-7 text-white">
              Cards you want
            </h2>
            <p className="mt-1 text-sm leading-6 text-gray-300">
              Select from the catalog for the cards you want
            </p>
            {wantsTrue ? (
              <MissingCards message={"Select cards you want"} />
            ) : null}
            <div className="flex flex-1 items-center justify-center px-2 lg:ml-6 lg:justify-end">
              <div className="w-full max-w-lg lg:max-w-xs">
                <label htmlFor="searchWants" className="sr-only">
                  Search
                </label>
                <div className="relative">
                  <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                    <MagnifyingGlassIcon
                      className="h-5 w-5 text-gray-400"
                      aria-hidden="true"
                    />
                  </div>
                  <input
                    onChange={handleSearchWantsChange}
                    value={searchWants}
                    id="searchWants"
                    name="searchWants"
                    className="block w-full rounded-md border-0 bg-gray-700 py-1.5 pl-10 pr-3 text-white ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    placeholder="Search"
                    type="search"
                  />
                </div>
              </div>
            </div>
            <div className="flex justify-center py-4">
              <div className="sm:col-span-3">
                <fieldset
                  onChange={handleWantsChange}
                  value={wants}
                  required
                  name="wants"
                  id="wants"
                >
                  <legend key="legend" className="sr-only">
                    Cards in your collection
                  </legend>
                  <div className="grid mobile:grid-cols-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-4 2xl:grid-cols-4 overflow-y-auto h-96">
                    {allCards
                      .filter((card) => {
                        if (
                          searchWants !== "" &&
                          card.card_name
                            .toUpperCase()
                            .includes(searchWants.toUpperCase())
                        ) {
                          return card;
                        } else if (searchWants !== "") {
                          return null;
                        } else {
                          return card;
                        }
                      })
                      .map((card) => {
                        return <PostCard card={card} checked={card.checked} />;
                      })}
                  </div>
                </fieldset>
              </div>
            </div>
          </div>
        </div>

        <div className="mt-6 flex items-center justify-end gap-x-6">
          <button
            type="submit"
            className="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
          >
            Create
          </button>
        </div>
      </form>
    </div>
  );
};

export default CreatePost;
