/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import InDepthView from "./components/InDepthView";

const CardDetails = () => {
  const [card, setCard] = useState();
  const [toggle, setToggle] = useState(false);

  const toggleAds = () => {
    setToggle(!toggle);
  };

  const getCard = async (id) => {
    const response = await fetch(
      `${process.env.REACT_APP_API_HOST}/doomlings/catalog/${id}`
    );
    const data = await response.json();
    setCard(data);
  };

  const params = useParams();

  useEffect(() => {
    getCard(Number(params.id));
  }, []);

  if (card === undefined) {
    return (
      <div>Please be patient your card is being delivered by Terror Beaks</div>
    );
  }

  return (
    <>
      <button
        className="rounded-md bg-indigo-500 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm
       hover:bg-indigo-400 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2
        focus-visible:outline-indigo-500"
        style={{
          backgroundColor: "#6a7380",
        }}
        onClick={toggleAds}
      >
        Enable Ads
      </button>
      {toggle ? (
        <div
          className="
            lg:basis-1/6 xl:basis-1/6 2xl:basis-1/6
            lg:fixed lg:top-1/3 lg:left-12 lg:w-width-lg
            xl:fixed xl:top-1/3 xl:left-6 xl:w-width-xl
            2xl:fixed 2xl:top-1/4 2xl:left-10 2xl:w-width-2xl"
        >
          <img
            src={`${process.env.PUBLIC_URL}/DIFFERENTPERFECT.png`}
            alt=""
            className="rounded"
          />
        </div>
      ) : null}
      <div>
        <InDepthView card={card} />
      </div>
      {toggle ? (
        <div
          className="
            lg:basis-1/6 xl:basis-1/6 2xl:basis-1/6
            lg:fixed lg:top-1/3 lg:right-12 lg:w-right-img
            xl:fixed xl:top-1/3 xl:right-10 xl:w-right-img
            2xl:fixed 2xl:w-width-2xl 2xl:top-44 2xl:right-5"
        >
          <img
            alt=""
            className="rounded"
            src={`${process.env.PUBLIC_URL}/SUPERPERFECT.png`}
          />
        </div>
      ) : null}
    </>
  );
};

export default CardDetails;
