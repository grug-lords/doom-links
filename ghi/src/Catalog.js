/* eslint-disable react-hooks/exhaustive-deps */
import DropDown from "./components/DropDown";

const Catalog = ({ allCards }) => {
  return (
    <>
      <div>
        <DropDown data={allCards} />
      </div>
    </>
  );
};

export default Catalog;
