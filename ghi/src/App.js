import React from "react";
import { useEffect, useState } from "react";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import Nav from "./Nav";
import LoginForm from "./LoginForm";
import Catalog from "./Catalog";
import CardDetails from "./CardDetails";
import ViewProfile from "./ViewProfile";
import UpdateAccountForm from "./UpdateAccountForm";
import CreateAccountForm from "./CreateAccountForm";
import CreatePost from "./CreatePost";
import CollectionView from "./components/CollectionView";
import Home from "./Home";
import ForumPosts from "./ForumPosts";

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");
  const [allCards, setAllCards] = useState([]);
  const [users, setUsers] = useState([]);


  const getAllCards = async () => {
    const response = await fetch(
      `${process.env.REACT_APP_API_HOST}/doomlings/catalog/`
    );
    const data = await response.json();
    setAllCards(data);
  };
  async function getUsers() {
    const response = await fetch(`${process.env.REACT_APP_API_HOST}/users`);
    const data = await response.json();
    setUsers(data);
  }
  useEffect(() => {
    getAllCards();
    getUsers();
  }, []);

  return (
    <AuthProvider baseUrl={process.env.REACT_APP_API_HOST}>
      <BrowserRouter basename={basename}>
        <header style={{ position: "sticky", top: "0", zIndex: "999" }}>
          <Nav />
        </header>
        <div>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="login" element={<LoginForm />} />
            <Route path="logout" element={<LoginForm />} />
            <Route path="doomlings/catalog">
              <Route index element={<Catalog allCards={allCards} />} />
              <Route path=":id" element={<CardDetails />} />
            </Route>
            <Route path="users">
              <Route
                path=":username"
                element={<ViewProfile users={users} />}
              />
              <Route path="edit" element={<UpdateAccountForm />} />
            </Route>
            <Route path="accounts" element={<CreateAccountForm />} />
            <Route path="doomlings/posts">
              <Route index element={<ForumPosts cards={allCards} />} />
              <Route path="create" element={<CreatePost cards={allCards} />} />
            </Route>
            <Route
              path="doomlings/collection"
              element={<CollectionView />}
            />
          </Routes>
        </div>
      </BrowserRouter>
    </AuthProvider>
  );
}

export default App;
