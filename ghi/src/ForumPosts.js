/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";
import Post from "./components/Post";
import { PlusIcon } from "@heroicons/react/20/solid";

const ForumPosts = ({ cards }) => {
  const { token } = useAuthContext();
  const navigate = useNavigate();
  const [posts, setPosts] = useState({});
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [last, setLast] = useState(1);
  const [toggle, setToggle] = useState(false);

  const toggleAds = () => {
    setToggle(!toggle);
  };

  const getPosts = async () => {
    if (Object.keys(cards).length > 0) {
      const response = await fetch(`${process.env.REACT_APP_API_HOST}/posts`);
      const data = await response.json();

      const postDict = {};
      let count = 1;
      let sum = 0;
      for (let post of data) {
        if (post["status"] === "created") {
          let images = [];
          for (let imgNum of post["trading"]) {
            if (imgNum <= 378) {
              images.push(cards[parseInt(imgNum) - 1]["image"]);
            } else if (imgNum >= 381) {
              images.push(cards[parseInt(imgNum) + 7]["image"]);
            } else {
              if (Math.floor(imgNum) === 379) {
                let index = Math.floor((imgNum - 379) * 10) - 1;
                images.push(cards[parseInt(index)]["image"]);
              } else {
                let index = Math.floor((imgNum - 380) * 10) + 3;
                images.push(cards[parseInt(index)]["image"]);
              }
            }
          }

          post["trading_img"] = images;
          images = [];
          for (let imgNum of post["wants"]) {
            if (imgNum <= 378) {
              images.push(cards[parseInt(imgNum) - 1]["image"]);
            } else if (imgNum >= 381) {
              images.push(cards[parseInt(imgNum) + 7]["image"]);
            } else {
              if (Math.floor(imgNum) === 379) {
                let index = Math.floor((imgNum - 379) * 10) - 1;
                images.push(cards[parseInt(index)]["image"]);
              } else {
                let index = Math.floor((imgNum - 380) * 10) + 3;
                images.push(cards[parseInt(index)]["image"]);
              }
            }
          }

          post["wants_img"] = images;
          if (postDict[count] === undefined) {
            postDict[count] = [post];
          } else {
            postDict[count].push(post);
            if (postDict[count].length === 15) {
              count++;
            }
          }
          sum++;
        }
      }
      setPosts(postDict);
      setTotal(sum);
      setLast(count);
    }
  };

  useEffect(() => {
    getPosts();
  }, [cards]);

  return (
    <div className="py-6">
      <div className="grid grid-cols-2 w-full">
        <div className="py-1 px-4 flex justify-start">
          <button
            className="rounded-md bg-indigo-500 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm
       hover:bg-indigo-400 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2
        focus-visible:outline-indigo-500"
            style={{
              backgroundColor: "#6a7380",
            }}
            onClick={toggleAds}
          >
            Enable Ads
          </button>
        </div>
        <div className="py-1 px-4 flex justify-end">
          {token ? (
            <button
              type="button"
              onClick={() => {
                navigate("/doomlings/posts/create");
              }}
              className="rounded-full bg-indigo-600 p-2 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
            >
              <PlusIcon className="h-5 w-5" aria-hidden="true" />
            </button>
          ) : null}
        </div>
      </div>
      <div
        className="px-2 flex mobile:flex-col mobile:items-center sm:flex-col sm:items-center md:flex-col md:items-center
      lg:justify-center lg:flex-row xl:flex-row 2xl:flex-row"
      >
        {toggle ? (
          <div
            className="
            lg:basis-1/6 xl:basis-1/6 2xl:basis-1/6
            lg:fixed lg:top-1/3 lg:left-12 lg:w-width-lg
            xl:fixed xl:top-1/3 xl:left-6 xl:w-width-xl
            2xl:fixed 2xl:top-1/4 2xl:left-10 2xl:w-width-2xl"
          >
            <img
              src={`${process.env.PUBLIC_URL}/PERFECT.png`}
              alt=""
              className="rounded"
            />
          </div>
        ) : null}
        <div className="grid grid-cols-1 py-3 md:w-2/3 lg:w-auto xl:w-auto 2xl:w-auto">
          {Object.keys(posts).length !== 0
            ? posts[page].map((post) => {
                return <Post post={post} />;
              })
            : null}
        </div>

        {toggle ? (
          <div
            className="
            lg:basis-1/6 xl:basis-1/6 2xl:basis-1/6
            lg:fixed lg:top-1/3 lg:right-12 lg:w-right-img
            xl:fixed xl:top-1/3 xl:right-10 xl:w-right-img
            2xl:fixed 2xl:w-width-2xl 2xl:top-44 2xl:right-5"
          >
            <img
              alt=""
              className="rounded"
              src={`${process.env.PUBLIC_URL}/PERFECBUTDIFFERENT.png`}
            />
          </div>
        ) : null}
      </div>

      <div className="pages flex justify-center py-5">
        <ul className="pagination pagination-sm">
          <li className="first px-1">
            <button
              type="button"
              className="rounded-full bg-indigo-600 p-2 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
              onClick={() => {
                setPage(1);
              }}
            >
              « First
            </button>
          </li>
          <li className="prev px-1">
            <button
              type="button"
              className="rounded-full bg-indigo-600 p-2 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
              onClick={() => {
                if (page !== 1) {
                  let pageCount = page - 1;
                  setPage(pageCount);
                }
              }}
            >
              «
            </button>
          </li>
          {page - 1 >= 1 ? (
            <li className="px-1">
              <button
                type="button"
                className="rounded-full bg-indigo-600 p-2 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                onClick={() => {
                  let pageCount = page - 1;
                  setPage(pageCount);
                }}
              >
                {page - 1}
              </button>
            </li>
          ) : null}

          <li className="px-1">
            <button
              type="button"
              className="rounded-full bg-indigo-600 p-2 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
            >
              {page}
            </button>
          </li>
          {page + 1 <= last ? (
            <li className="px-1">
              <button
                type="button"
                className="rounded-full bg-indigo-600 p-2 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                onClick={() => {
                  let pageCount = page + 1;
                  setPage(pageCount);
                }}
              >
                {page + 1}
              </button>
            </li>
          ) : null}
          {page + 2 <= last ? (
            <li className="px-1">
              <button
                type="button"
                className="rounded-full bg-indigo-600 p-2 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                onClick={() => {
                  let pageCount = page + 2;
                  setPage(pageCount);
                }}
              >
                {page + 2}
              </button>
            </li>
          ) : null}
          {page + 3 <= last ? (
            <li className="px-1">
              <button
                type="button"
                className="rounded-full bg-indigo-600 p-2 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                onClick={() => {
                  let pageCount = page + 3;
                  setPage(pageCount);
                }}
              >
                {page + 3}
              </button>
            </li>
          ) : null}

          <li className="next px-1">
            <button
              type="button"
              className="rounded-full bg-indigo-600 p-2 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
              onClick={() => {
                if (page !== last) {
                  let pageCount = page + 1;
                  setPage(pageCount);
                }
              }}
            >
              »
            </button>
          </li>
          <li className="last px-1">
            <button
              type="button"
              onClick={() => {
                setPage(last);
              }}
              className="rounded-full bg-indigo-600 p-2 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
            >
              Last »
            </button>
          </li>
          {page * 15 <= total ? (
            <li className="px-1">
              {page * 15} of {total}
            </li>
          ) : (
            <li className="px-1">
              {total} of {total}
            </li>
          )}
        </ul>
      </div>
    </div>
  );
};

export default ForumPosts;
