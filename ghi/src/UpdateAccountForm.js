import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router";
import { useState } from "react";

export default function UpdateAccountForm() {
  const { token } = useToken();
  const navigate = useNavigate();

  const [location, setLocation] = useState("");
  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  };

  const [contactInfo, setContactInfo] = useState("");
  const handleContactInfoChange = (event) => {
    const value = event.target.value;
    setContactInfo(value);
  };

  const [avatar, setAvatar] = useState("");
  const handleAvatarChange = async (event) => {
    const file = event.target.files[0];
    setAvatar(file);
  };

  const [alert, setAlert] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();
    let data = {};
    data.location = location;
    data.contact_info = contactInfo;

    if (data.location || data.contact_info) {
      const fetchConfig = {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
      const url = `${process.env.REACT_APP_API_HOST}/users/self`;
      const response = await fetch(url, fetchConfig);
      let responseMessage = await response.json();

      if (response.status === 400) {
        setAlert(responseMessage);
      } else if (response.ok) {
        setLocation("");
        setContactInfo("");
        setAvatar("");
        navigate("/users/self");
      }
    }

    if (avatar) {
      let formData = new FormData();
      formData.append("file", avatar);
      const fetchConfig = {
        method: "PUT",
        body: formData,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
      const url = `${process.env.REACT_APP_API_HOST}/users/self/profile`;
      const avatarResponse = await fetch(url, fetchConfig);
      let avatarResponseMessage = await avatarResponse.json();

      if (avatarResponse.status === 400) {
        setAlert(avatarResponseMessage);
      } else if (avatarResponse.ok) {
        setLocation("");
        setContactInfo("");
        setAvatar("");
        navigate("/users/self");
      }
    }
  };

  return (
    <div className="flex py-10 justify-center">
      <form onSubmit={handleSubmit} id="updateform">
        <div className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
          <div className="mb-4">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="location"
            >
              Location
            </label>
            <input
              onChange={handleLocationChange}
              type="text"
              id="location"
              name="location"
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              value={location}
              placeholder="City, Country"
            />
          </div>
          <div className="mb-4">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="contact"
            >
              Contact Info
            </label>
            <textarea
              onChange={handleContactInfoChange}
              type="text"
              id="contact"
              name="contact"
              rows={3}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              value={contactInfo}
              placeholder="How other users can contact you"
            />
          </div>
          <div className="mb-6">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="avatar"
            >
              Avatar
            </label>
            <input
              onChange={handleAvatarChange}
              type="file"
              accept="image/*"
              id="avatar"
              name="avatar"
              className="w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div className="flex items-center justify-between">
            <button
              className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              type="submit"
            >
              Update
            </button>
          </div>
          {alert ? (
            <div
              className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
              role="alert"
            >
              <strong className="font-bold">Yikes!</strong>
              <span className="block sm:inline"> {alert.detail}.</span>
              <span className="absolute top-0 bottom-0 right-0 px-4 py-3">
                <svg
                  className="fill-current h-6 w-6 text-red-500"
                  role="button"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 20 20"
                >
                  <title>Close</title>
                  <path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z" />
                </svg>
              </span>
            </div>
          ) : (
            <></>
          )}
        </div>
      </form>
    </div>
  );
}
