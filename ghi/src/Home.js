import { useNavigate } from "react-router-dom";

export default function Home() {
  const navigate = useNavigate();

  const handleCatalogClick = () => {
    navigate("/doomlings/catalog");
  };
  const handleForumClick = () => {
    navigate("/doomlings/posts");
  };

  return (
    <>
      <div className="flex justify-center h-20"></div>
      <div className="grid grid-cols-2 justify-content-between justify-items-center">
        <div className="">
          <button
            onClick={() => {
              handleCatalogClick();
            }}
          >
            <h1 className="text-3xl font-bold text-gray-100">Catalog</h1>
            <img
              className="h-75%"
              src={`${process.env.PUBLIC_URL}/catalog.jpg`}
              alt=""
            />
          </button>
        </div>
        <div>
          <button
            onClick={() => {
              handleForumClick();
            }}
          >
            <h1 className="text-3xl font-bold text-gray-100">Forum</h1>
            <img
              className="h-75%"
              src={`${process.env.PUBLIC_URL}/forum.jpg`}
              alt=""
            />
          </button>
        </div>
      </div>
    </>
  );
}
