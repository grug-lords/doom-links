/* eslint-disable react-hooks/exhaustive-deps */
import Profile from "./components/Profile";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useParams } from "react-router";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router";

const ViewProfile = () => {
  const { token } = useToken();
  const { username } = useParams();
  const navigate = useNavigate();

  const handleClick = async (event) => {
    event.preventDefault();
    navigate("/users/edit");
  };

  const [profileData, setProfileData] = useState({ username: null });
  async function getProfileData() {
    const config = {
      method: "GET",
      credentials: "include",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(
      `${process.env.REACT_APP_API_HOST}/users/${username}`,
      config
    );
    const data = await response.json();
    setProfileData(data);
  }

  const [user, setUser] = useState();
  async function getUser() {
    const config = {
      method: "GET",
      credentials: "include",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(
      `${process.env.REACT_APP_API_HOST}/users/self`,
      config
    );
    const data = await response.json();
    setUser(data.username);
  }

  useEffect(() => {
    if (token) {
      getUser();
      getProfileData();
    }
  }, [token]);

    return (
      <>
        {token && profileData.username === user ? (
          <>
            <div className="flex px-2 py-2 justify-end">
              <button
                onClick={handleClick}
                className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                type="edit"
              >
                Edit my profile
              </button>
            </div>
            <div>
              <Profile userdata={profileData} />
            </div>
          </>
        ) : (
          <>
            <div>
              <Profile userdata={profileData} />
            </div>
          </>
        )}
      </>
    );
}

export default ViewProfile;
