import { NavLink } from "react-router-dom";

export default function Cardview({ data }) {
  return (
    <div>
      <div
        style={{
          marginLeft: "10%",
          marginRight: "10%",
          marginTop: "3%",
          marginBottom: "3%",
        }}
      >
        <h2 className="sr-only">Cards</h2>

        <div className="grid grid-cols-2 gap-x-6 gap-y-10 sm:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 xl:gap-x-8">
          {data.map((card) => (
            <NavLink
              to={`${card.card_id}`}
              key={card.card_id}
              className="group"
            >
              <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-lg bg-gray-200 xl:aspect-h-8 xl:aspect-w-7">
                <img
                  src={card.image}
                  alt={card.name}
                  className="h-full w-full object-cover object-center group-hover:opacity-75"
                />
              </div>
            </NavLink>
          ))}
        </div>
      </div>
    </div>
  );
}
