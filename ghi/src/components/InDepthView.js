/* eslint-disable react-hooks/exhaustive-deps */
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useState, useEffect } from "react";

export default function InDepthView({ card }) {
  const { token } = useToken();
  const [owned, setOwned] = useState(false);
  const [userCards, setUserCards] = useState([]);

  const addToCollection = async (e) => {
    e.preventDefault();

    const collectionUrl = `${process.env.REACT_APP_API_HOST}/doomlings/collection/`;

    const data = {
      card_id: card.card_id,
      trading: false,
      amount: 1,
      holos: 0,
    };

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(collectionUrl, fetchConfig);
    if (!response.ok) {
      console.log("Bad request");
    }
    fetchUserCollection();
  };

  const fetchUserCollection = async () => {
    try {
      if (token) {
        const config = {
          method: "GET",
          credentials: "include",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        };
        const response = await fetch(
          `${process.env.REACT_APP_API_HOST}/doomlings/collection`,
          config
        );
        const data = await response.json();
        setUserCards(data);
      } else {
        setUserCards(null);
      }
    } catch (error) {}
  };

  const checkOwned = async () => {
    if (token) {
      const config = {
        method: "GET",
        credentials: "include",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
      const response = await fetch(
        `${process.env.REACT_APP_API_HOST}/doomlings/collection/${card.card_id}/`,
        config
      );
      const card_id = await response.json();
      if (card_id.card_id === card.card_id) {
        setOwned(true);
      }
    }
  };

  useEffect(() => {
    if (token) {
      fetchUserCollection();
    }
  }, []);

  useEffect(() => {
    checkOwned();
  }, [userCards]);

  return (
    <>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          marginLeft: "20%",
          flexWrap: "wrap",
          marginTop: "3%",
          marginRight: "20%",
          marginBottom: "3%",
          // textAlign: "center",
        }}
      >
        {/* Product image */}
        <div
          className="mt-10 lg:col-start-2 lg:row-span-2 lg:mt-0 lg:self-center"
          style={{ flexBasis: "50%" }}
        >
          <div
            className="aspect-h-1 aspect-w-1 overflow-hidden rounded-lg"
            style={{ marginLeft: "10%" }}
          >
            <img
              style={{ width: "375px" }}
              src={card.image}
              alt="..."
              className="h-full w-full object-cover object-center"
            />
          </div>
        </div>

        <div
          className="mx-auto max-w-2xl px-1 py-2 sm:px-6 sm:py-24 lg:max-w-7xl lg:gap-x-8 lg:px-8"
          style={{ flexBasis: "50%" }}
        >
          {/* Product details */}

          <div className="mt-4">
            <h1
              className="text-3xl font-bold tracking-tight sm:text-4xl"
              style={{ color: "rgba(255, 255, 255, 1)" }}
            >
              {card.card_name}
            </h1>

            <section
              aria-labelledby="information-heading"
              className="mt-4"
              // style={{ textAlign: "center" }}
            >
              <h2
                id="information-heading"
                className="sr-only"
                style={{ color: "rgba(255, 255, 255, 1)" }}
              >
                Collection ID
              </h2>

              <div
                className="flex items-center"
                // style={{ justifyContent: "center" }}
              >
                <div className="text-lg sm:text-xl">{card.card_id}</div>
                <div className="ml-4 border-l border-gray-300 pl-4">
                  <h2
                    className="sr-only"
                    style={{ color: "rgba(255, 255, 255, 1)" }}
                  >
                    Rarity
                  </h2>
                  <div className="flex items-center">
                    <div className="text-xl sm:text-xl"> {card.rarity}</div>
                  </div>
                </div>
              </div>

              <ul
                className="mt-4 space-y-6"
                style={{
                  display: "flex",
                  justifyContent: "flex-start",
                  flexDirection: "column",
                  paddingLeft: "0",
                }}
              >
                <li className="text-xl text-slate-400">
                  <h6 style={{ color: "#e4e7eb" }}>&nbsp;Collection: </h6>
                  &nbsp;&nbsp;&nbsp;
                  {card.collection}
                </li>
                {card.sub_collection !== null && (
                  <li className="text-xl text-slate-400">
                    <h6 style={{ color: "#e4e7eb" }}>&nbsp;Sub-Collection: </h6>
                    &nbsp; &nbsp;&nbsp;
                    {card.sub_collection}
                  </li>
                )}
                <li className="text-xl text-slate-400">
                  <h6 style={{ color: "#e4e7eb" }}>&nbsp;Type: </h6>
                  &nbsp;&nbsp;&nbsp;
                  {card.card_type}
                </li>
                <li className="text-xl text-slate-400">
                  <h6 style={{ color: "#e4e7eb" }}>&nbsp;Sub-Type: </h6>
                  &nbsp;&nbsp;&nbsp;
                  {card.sub_type}
                </li>
                <li className="text-xl text-slate-400">
                  <h6 style={{ color: "#e4e7eb" }}>&nbsp;Color: </h6>
                  &nbsp;&nbsp;&nbsp;
                  {card.color}
                </li>
                <li className="text-xl text-slate-400">
                  <h6 style={{ color: "#e4e7eb" }}>&nbsp;Points: </h6>
                  &nbsp;&nbsp;&nbsp;
                  {card.points}
                </li>
              </ul>
            </section>
          </div>
        </div>
        {/* Product form */}
        {token ? (
          owned ? (
            <div
              style={{
                display: "flex",
                width: "100%",
                justifyContent: "center",
                gap: "20%",
              }}
            >
              <div className="mt-10 lg:col-start-1 lg:row-start-2 lg:max-w-lg lg:self-start">
                <div className="mt-10">
                  <button
                    onClick={addToCollection}
                    type="submit"
                    className="flex w-full items-center justify-center rounded-md border border-transparent bg-indigo-600 px-8 py-3 text-base font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 focus:ring-offset-gray-50"
                  >
                    Add or Remove
                  </button>
                </div>
              </div>
              <div className="mt-10 lg:col-start-1 lg:row-start-2 lg:max-w-lg lg:self-start">
                <div className="mt-10">
                  <button
                    onClick={addToCollection}
                    type="submit"
                    className="flex w-full items-center justify-center rounded-md border border-transparent bg-indigo-600 px-8 py-3 text-base font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 focus:ring-offset-gray-50"
                  >
                    Add or Remove Holos
                  </button>
                </div>
              </div>
            </div>
          ) : (
            <div
              className="mt-10 lg:col-start-1 lg:row-start-2 lg:max-w-lg lg:self-start"
              style={{ flexBasis: "100%" }}
            >
              <div className="mt-10">
                <button
                  onClick={addToCollection}
                  type="submit"
                  className="flex w-full items-center justify-center rounded-md border border-transparent bg-indigo-600 px-8 py-3 text-base font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 focus:ring-offset-gray-50"
                >
                  Add to Collection
                </button>
              </div>
            </div>
          )
        ) : null}
      </div>
    </>
  );
}
