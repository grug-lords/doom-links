/* eslint-disable react-hooks/exhaustive-deps */
import { NavLink } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useEffect, useState } from "react";

export default function CollectionView() {
  const { token } = useToken();
  const [userCards, setUserCards] = useState([]);

  const fetchUserCollection = async () => {
    try {
      if (token) {
        const config = {
          method: "GET",
          credentials: "include",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        };
        const response = await fetch(
          `${process.env.REACT_APP_API_HOST}/doomlings/collection`,
          config
        );
        const data = await response.json();
        setUserCards(data);
      }
    } catch (error) {}
  };

  useEffect(() => {
    fetchUserCollection();
  }, [token]);

  return (
    <div className="">
      <div
        style={{
          marginLeft: "10%",
          marginRight: "10%",
          marginTop: "3%",
          marginBottom: "3%",
        }}
      >
        {" "}
        <p> My Collection : </p>
        <h2 className="sr-only">Cards</h2>
        <div className="grid grid-cols-2 gap-x-6 gap-y-10 sm:grid-cols-3 lg:grid-cols-6 xl:grid-cols-6 xl:gap-x-8">
          {userCards.map((userCard) => (
            <NavLink
              to={`${process.env.PUBLIC_URL}/doomlings/catalog/${userCard.card_id}`}
              key={userCard.card_id}
              className="group"
            >
              <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-lg bg-gray-200 xl:aspect-h-8 xl:aspect-w-7">
                <img
                  src={userCard.image}
                  alt={userCard.name}
                  className="h-full w-full object-cover object-center group-hover:opacity-75"
                />
              </div>
            </NavLink>
          ))}
        </div>
      </div>
    </div>
  );
}
