/* eslint-disable react-hooks/exhaustive-deps */
import { Fragment, useState, useEffect } from "react";
import { Listbox, Transition } from "@headlessui/react";
import { ChevronUpDownIcon } from "@heroicons/react/20/solid";

import CardView from "./CardView";
import {
  placeholder,
  colors,
  types,
  points,
  subtypes,
  collections,
  rarity,
} from "./DDvariables";
function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function DropDown({ data }) {
  const [selectedColor, setSelectedColor] = useState(placeholder[0]);
  const [selectedType, setSelectedType] = useState(placeholder[1]);
  const [selectedPoints, setSelectedPoints] = useState(placeholder[2]);
  const [selectedSubType, setSelectedSubType] = useState(placeholder[3]);
  const [selectedCollection, setSelectedCollection] = useState(placeholder[4]);
  const [selectedRarity, setSelectedRarity] = useState(placeholder[5]);
  const [filteredCards, setFilteredCards] = useState(data);
  const [filterChoices, setFilterChoices] = useState({});

  const getCards = () => {
    setFilteredCards(data);
  };

  const filterCards = () => {
    let result = data;
    for (let key in filterChoices) {
      if (filterChoices[key] !== undefined) {
        result = result.filter((card) => {
          if (key === "rarity") {
            return card[key].includes(filterChoices[key]);
          }
          if (key === "sub_type" && card[key]) {
            return card[key].includes(filterChoices[key]);
          }
          if (key === "color") {
            return card[key].includes(filterChoices[key]);
          }
          if (key === "points" && filterChoices[key] !== "variable") {
            if (
              filterChoices[key] === "6 or More" &&
              parseInt(card[key]) >= 6
            ) {
              return card;
            } else if (
              filterChoices[key] === "Zero or Less" &&
              parseInt(card[key]) <= 0
            ) {
              return card;
            }
          }
          return card[key] === filterChoices[key];
        });
      }
    }
    setFilteredCards(result);
  };

  const handleFilterChange = (event) => {
    const value = event.name;
    const inputName = event.value;
    setFilterChoices({
      ...filterChoices,

      [inputName]: value,
    });
  };

  const colorChange = (event) => {
    setSelectedColor(event);
    handleFilterChange(event);
  };
  const typeChange = (event) => {
    setSelectedType(event);
    handleFilterChange(event);
  };
  const pointsChange = (event) => {
    setSelectedPoints(event);
    handleFilterChange(event);
  };
  const subTypeChange = (event) => {
    setSelectedSubType(event);
    handleFilterChange(event);
  };
  const collectionChange = (event) => {
    setSelectedCollection(event);
    handleFilterChange(event);
  };
  const rarityChange = (event) => {
    setSelectedRarity(event);
    handleFilterChange(event);
  };

  const resetFilter = () => {
    setFilterChoices({});
    setSelectedColor(placeholder[0]);
    setSelectedType(placeholder[1]);
    setSelectedPoints(placeholder[2]);
    setSelectedSubType(placeholder[3]);
    setSelectedCollection(placeholder[4]);
    setSelectedRarity(placeholder[5]);
    getCards();
  };

  useEffect(() => {
    getCards();
  }, [data]);

  useEffect(() => {
    filterCards();
  }, [filterChoices]);

  return (
    <>
      <div
        style={{
          position: "sticky",
          top: "64px",
          zIndex: "998",
          backgroundColor: "#282c34",
          paddingBottom: "10px",
        }}
      >
        <div className="flex justify-center space-x-2 gap-2 flex-wrap flex-start">
          <div>
            <Listbox value={selectedColor} onChange={colorChange}>
              {({ open }) => (
                <>
                  <Listbox.Label
                    className="block text-sm font-medium leading-6 "
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      marginTop: "5px",
                    }}
                  >
                    Color
                  </Listbox.Label>
                  <div className="relative mt-2">
                    <Listbox.Button className="relative w-full cursor-default rounded-md  py-1.5 pl-3 pr-10 text-left  ring-1 ring-inset ring-gray-100 focus:outline-none focus:ring-2 focus:ring-indigo-500 sm:text-sm sm:leading-6">
                      <span className="flex items-center">
                        <img
                          src={selectedColor.avatar}
                          alt=""
                          className="h-5 w-5 flex-shrink-0 rounded-full"
                        />
                        <span className="ml-3 block truncate">
                          {selectedColor.name}
                        </span>
                      </span>
                      <span className="pointer-events-none absolute inset-y-0 right-0 ml-3 flex items-center pr-2">
                        <ChevronUpDownIcon
                          className="h-5 w-5 text-gray-400"
                          aria-hidden="true"
                        />
                      </span>
                    </Listbox.Button>
                    <Transition
                      show={open}
                      as={Fragment}
                      leave="transition ease-in duration-100"
                      leaveFrom="opacity-100"
                      leaveTo="opacity-0"
                    >
                      <Listbox.Options className="absolute z-10 mt-1 max-h-56 w-full overflow-auto rounded-md  py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                        {colors.map((color) => (
                          <Listbox.Option
                            key={color.id}
                            className={({ active }) =>
                              classNames(
                                active
                                  ? "bg-indigo-600 text-white"
                                  : "bg-neutral-900",
                                "relative cursor-default select-none py-2 pl-3 pr-9"
                              )
                            }
                            value={color}
                          >
                            {({ selectedColor, active }) => (
                              <>
                                <div className="flex items-center">
                                  <img
                                    src={color.avatar}
                                    alt=""
                                    className="h-5 w-5 flex-shrink-0 rounded-full"
                                  />
                                  <span
                                    className={classNames(
                                      selectedColor
                                        ? "font-semibold"
                                        : "font-normal",
                                      "ml-3 block truncate"
                                    )}
                                  >
                                    {color.name}
                                  </span>
                                </div>
                                {selectedColor ? (
                                  <span
                                    className={classNames(
                                      active ? "text-white" : "text-indigo-600",
                                      "absolute inset-y-0 right-0 flex items-center pr-4"
                                    )}
                                  ></span>
                                ) : null}
                              </>
                            )}
                          </Listbox.Option>
                        ))}
                      </Listbox.Options>
                    </Transition>
                  </div>
                </>
              )}
            </Listbox>
          </div>
          <div>
            <Listbox value={selectedType} onChange={typeChange}>
              {({ open }) => (
                <>
                  <Listbox.Label
                    className="block text-sm font-medium leading-6 "
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      marginTop: "5px",
                    }}
                  >
                    Type
                  </Listbox.Label>
                  <div className="relative mt-2">
                    <Listbox.Button className="relative w-full cursor-default rounded-md  py-1.5 pl-3 pr-10 text-left  ring-1 ring-inset ring-gray-100 focus:outline-none focus:ring-2 focus:ring-indigo-500 sm:text-sm sm:leading-6">
                      <span className="flex items-center">
                        <img
                          src={selectedType.avatar}
                          alt=""
                          className="h-5 w-5 flex-shrink-0 rounded-full"
                        />
                        <span className="ml-3 block truncate">
                          {selectedType.name}
                        </span>
                      </span>
                      <span className="pointer-events-none absolute inset-y-0 right-0 ml-3 flex items-center pr-2">
                        <ChevronUpDownIcon
                          className="h-5 w-5 text-gray-400"
                          aria-hidden="true"
                        />
                      </span>
                    </Listbox.Button>
                    <Transition
                      show={open}
                      as={Fragment}
                      leave="transition ease-in duration-100"
                      leaveFrom="opacity-100"
                      leaveTo="opacity-0"
                    >
                      <Listbox.Options className="absolute z-10 mt-1 max-h-56 w-full overflow-auto rounded-md  py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                        {types.map((type) => (
                          <Listbox.Option
                            key={type.id}
                            className={({ active }) =>
                              classNames(
                                active
                                  ? "bg-indigo-600 text-white"
                                  : "bg-neutral-900",
                                "relative cursor-default select-none py-2 pl-3 pr-9"
                              )
                            }
                            value={type}
                          >
                            {({ selectedType, active }) => (
                              <>
                                <div className="flex items-center">
                                  <img
                                    src={type.avatar}
                                    alt=""
                                    className="h-5 w-5 flex-shrink-0 rounded-full"
                                  />
                                  <span
                                    className={classNames(
                                      selectedType
                                        ? "font-semibold"
                                        : "font-normal",
                                      "ml-3 block truncate"
                                    )}
                                  >
                                    {type.name}
                                  </span>
                                </div>
                                {selectedType ? (
                                  <span
                                    className={classNames(
                                      active ? "text-white" : "text-indigo-600",
                                      "absolute inset-y-0 right-0 flex items-center pr-4"
                                    )}
                                  ></span>
                                ) : null}
                              </>
                            )}
                          </Listbox.Option>
                        ))}
                      </Listbox.Options>
                    </Transition>
                  </div>
                </>
              )}
            </Listbox>
          </div>
          <div>
            <Listbox value={selectedPoints} onChange={pointsChange}>
              {({ open }) => (
                <>
                  <Listbox.Label
                    className="block text-sm font-medium leading-6 "
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      marginTop: "5px",
                    }}
                  >
                    Points
                  </Listbox.Label>
                  <div className="relative mt-2">
                    <Listbox.Button className="relative w-full cursor-default rounded-md  py-1.5 pl-3 pr-10 text-left  ring-1 ring-inset ring-gray-100 focus:outline-none focus:ring-2 focus:ring-indigo-500 sm:text-sm sm:leading-6">
                      <span className="flex items-center">
                        <img
                          src={selectedPoints.avatar}
                          alt=""
                          className="h-5 w-5 flex-shrink-0 rounded-full"
                        />
                        <span className="ml-3 block truncate">
                          {selectedPoints.name}
                        </span>
                      </span>
                      <span className="pointer-events-none absolute inset-y-0 right-0 ml-3 flex items-center pr-2">
                        <ChevronUpDownIcon
                          className="h-5 w-5 text-gray-400"
                          aria-hidden="true"
                        />
                      </span>
                    </Listbox.Button>
                    <Transition
                      show={open}
                      as={Fragment}
                      leave="transition ease-in duration-100"
                      leaveFrom="opacity-100"
                      leaveTo="opacity-0"
                    >
                      <Listbox.Options className="absolute z-10 mt-1 max-h-56 w-full overflow-auto rounded-md  py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                        {points.map((point) => (
                          <Listbox.Option
                            key={point.id}
                            className={({ active }) =>
                              classNames(
                                active
                                  ? "bg-indigo-600 text-white"
                                  : "bg-neutral-900",
                                "relative cursor-default select-none py-2 pl-3 pr-9"
                              )
                            }
                            value={point}
                          >
                            {({ selectedPoints, active }) => (
                              <>
                                <div className="flex items-center">
                                  <img
                                    src={point.avatar}
                                    alt=""
                                    className="h-5 w-5 flex-shrink-0 rounded-full"
                                  />
                                  <span
                                    className={classNames(
                                      selectedPoints
                                        ? "font-semibold"
                                        : "font-normal",
                                      "ml-3 block truncate"
                                    )}
                                  >
                                    {point.name}
                                  </span>
                                </div>
                                {selectedType ? (
                                  <span
                                    className={classNames(
                                      active ? "text-white" : "text-indigo-600",
                                      "absolute inset-y-0 right-0 flex items-center pr-4"
                                    )}
                                  ></span>
                                ) : null}
                              </>
                            )}
                          </Listbox.Option>
                        ))}
                      </Listbox.Options>
                    </Transition>
                  </div>
                </>
              )}
            </Listbox>
          </div>
          <div>
            <Listbox value={selectedSubType} onChange={subTypeChange}>
              {({ open }) => (
                <>
                  <Listbox.Label
                    className="block text-sm font-medium leading-6 "
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      marginTop: "5px",
                    }}
                  >
                    SubType
                  </Listbox.Label>
                  <div className="relative mt-2">
                    <Listbox.Button className="relative w-full cursor-default rounded-md  py-1.5 pl-3 pr-10 text-left  ring-1 ring-inset ring-gray-100 focus:outline-none focus:ring-2 focus:ring-indigo-500 sm:text-sm sm:leading-6">
                      <span className="flex items-center">
                        <img
                          src={selectedSubType.avatar}
                          alt=""
                          className="h-5 w-5 flex-shrink-0 rounded-full"
                        />
                        <span className="ml-3 block truncate">
                          {selectedSubType.name}
                        </span>
                      </span>
                      <span className="pointer-events-none absolute inset-y-0 right-0 ml-3 flex items-center pr-2">
                        <ChevronUpDownIcon
                          className="h-5 w-5 text-gray-400"
                          aria-hidden="true"
                        />
                      </span>
                    </Listbox.Button>
                    <Transition
                      show={open}
                      as={Fragment}
                      leave="transition ease-in duration-100"
                      leaveFrom="opacity-100"
                      leaveTo="opacity-0"
                    >
                      <Listbox.Options className="absolute z-10 mt-1 max-h-56 w-full overflow-auto rounded-md  py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                        {subtypes.map((subtype) => (
                          <Listbox.Option
                            key={subtype.id}
                            className={({ active }) =>
                              classNames(
                                active
                                  ? "bg-indigo-600 text-white"
                                  : "bg-neutral-900",
                                "relative cursor-default select-none py-2 pl-3 pr-9"
                              )
                            }
                            value={subtype}
                          >
                            {({ selectedSubType, active }) => (
                              <>
                                <div className="flex items-center">
                                  <img
                                    src={subtype.avatar}
                                    alt=""
                                    className="h-5 w-5 flex-shrink-0 rounded-full"
                                  />
                                  <span
                                    className={classNames(
                                      selectedSubType
                                        ? "font-semibold"
                                        : "font-normal",
                                      "ml-3 block truncate"
                                    )}
                                  >
                                    {subtype.name}
                                  </span>
                                </div>
                                {selectedType ? (
                                  <span
                                    className={classNames(
                                      active ? "text-white" : "text-indigo-600",
                                      "absolute inset-y-0 right-0 flex items-center pr-4"
                                    )}
                                  ></span>
                                ) : null}
                              </>
                            )}
                          </Listbox.Option>
                        ))}
                      </Listbox.Options>
                    </Transition>
                  </div>
                </>
              )}
            </Listbox>
          </div>
          <div>
            <Listbox value={selectedCollection} onChange={collectionChange}>
              {({ open }) => (
                <>
                  <Listbox.Label
                    className="block text-sm font-medium leading-6 "
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      marginTop: "5px",
                    }}
                  >
                    Collection
                  </Listbox.Label>
                  <div className="relative mt-2">
                    <Listbox.Button className="relative w-full cursor-default rounded-md  py-1.5 pl-3 pr-10 text-left  ring-1 ring-inset ring-gray-100 focus:outline-none focus:ring-2 focus:ring-indigo-500 sm:text-sm sm:leading-6">
                      <span className="flex items-center">
                        <img
                          src={selectedCollection.avatar}
                          alt=""
                          className="h-5 w-5 flex-shrink-0 rounded-full"
                        />
                        <span className="ml-3 block truncate">
                          {selectedCollection.name}
                        </span>
                      </span>
                      <span className="pointer-events-none absolute inset-y-0 right-0 ml-3 flex items-center pr-2">
                        <ChevronUpDownIcon
                          className="h-5 w-5 text-gray-400"
                          aria-hidden="true"
                        />
                      </span>
                    </Listbox.Button>
                    <Transition
                      show={open}
                      as={Fragment}
                      leave="transition ease-in duration-100"
                      leaveFrom="opacity-100"
                      leaveTo="opacity-0"
                    >
                      <Listbox.Options className="absolute z-10 mt-1 max-h-56 w-full overflow-auto rounded-md  py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                        {collections.map((collection) => (
                          <Listbox.Option
                            key={collection.id}
                            className={({ active }) =>
                              classNames(
                                active
                                  ? "bg-indigo-600 text-white"
                                  : "bg-neutral-900",
                                "relative cursor-default select-none py-2 pl-3 pr-9"
                              )
                            }
                            value={collection}
                          >
                            {({ selectedCollection, active }) => (
                              <>
                                <div className="flex items-center">
                                  <img
                                    src={collection.avatar}
                                    alt=""
                                    className="h-5 w-5 flex-shrink-0 rounded-full"
                                  />
                                  <span
                                    className={classNames(
                                      selectedCollection
                                        ? "font-semibold"
                                        : "font-normal",
                                      "ml-3 block truncate"
                                    )}
                                  >
                                    {collection.name}
                                  </span>
                                </div>
                                {selectedCollection ? (
                                  <span
                                    className={classNames(
                                      active ? "text-white" : "text-indigo-600",
                                      "absolute inset-y-0 right-0 flex items-center pr-4"
                                    )}
                                  ></span>
                                ) : null}
                              </>
                            )}
                          </Listbox.Option>
                        ))}
                      </Listbox.Options>
                    </Transition>
                  </div>
                </>
              )}
            </Listbox>
          </div>
          <div>
            <Listbox value={selectedRarity} onChange={rarityChange}>
              {({ open }) => (
                <>
                  <Listbox.Label
                    className="block text-sm font-medium leading-6 "
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      marginTop: "5px",
                    }}
                  >
                    Rarity
                  </Listbox.Label>
                  <div className="relative mt-2">
                    <Listbox.Button className="relative w-full cursor-default rounded-md  py-1.5 pl-3 pr-10 text-left  ring-1 ring-inset ring-gray-100 focus:outline-none focus:ring-2 focus:ring-indigo-500 sm:text-sm sm:leading-6">
                      <span className="flex items-center">
                        <img
                          src={selectedRarity.avatar}
                          alt=""
                          className="h-5 w-5 flex-shrink-0 rounded-full"
                        />
                        <span className="ml-3 block truncate">
                          {selectedRarity.name}
                        </span>
                      </span>
                      <span className="pointer-events-none absolute inset-y-0 right-0 ml-3 flex items-center pr-2">
                        <ChevronUpDownIcon
                          className="h-5 w-5 text-gray-400"
                          aria-hidden="true"
                        />
                      </span>
                    </Listbox.Button>
                    <Transition
                      show={open}
                      as={Fragment}
                      leave="transition ease-in duration-100"
                      leaveFrom="opacity-100"
                      leaveTo="opacity-0"
                    >
                      <Listbox.Options className="absolute z-10 mt-1 max-h-56 w-full overflow-auto rounded-md  py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                        {rarity.map((rarity) => (
                          <Listbox.Option
                            key={rarity.id}
                            className={({ active }) =>
                              classNames(
                                active
                                  ? "bg-indigo-600 text-white"
                                  : "bg-neutral-900",
                                "relative cursor-default select-none py-2 pl-3 pr-9"
                              )
                            }
                            value={rarity}
                          >
                            {({ selectedRarity, active }) => (
                              <>
                                <div className="flex items-center">
                                  <img
                                    src={rarity.avatar}
                                    alt=""
                                    className="h-5 w-5 flex-shrink-0 rounded-full"
                                  />
                                  <span
                                    className={classNames(
                                      selectedRarity
                                        ? "font-semibold"
                                        : "font-normal",
                                      "ml-3 block truncate"
                                    )}
                                  >
                                    {rarity.name}
                                  </span>
                                </div>
                                {selectedRarity ? (
                                  <span
                                    className={classNames(
                                      active ? "text-white" : "text-indigo-600",
                                      "absolute inset-y-0 right-0 flex items-center pr-4"
                                    )}
                                  ></span>
                                ) : null}
                              </>
                            )}
                          </Listbox.Option>
                        ))}
                      </Listbox.Options>
                    </Transition>
                  </div>
                </>
              )}
            </Listbox>
          </div>
          <div>
            <button
              onClick={resetFilter}
              type="button"
              className="rounded-md bg-indigo-500 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm
       hover:bg-indigo-400 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2
        focus-visible:outline-indigo-500"
              style={{
                backgroundColor: "#6a7380",
                display: "flex",
                justifyContent: "center",
                marginTop: "35%",
              }}
            >
              Reset Filters
            </button>
          </div>
        </div>
      </div>
      <div>
        <CardView data={filteredCards} />
      </div>
    </>
  );
}
