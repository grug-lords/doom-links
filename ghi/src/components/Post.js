import React from "react";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";

const Post = ({ post }) => {
  const { token } = useAuthContext();
  return (
    <section
      key={post.id}
      className="py-2 md:w-md-scr lg:w-auto xl:w-auto 2xl:w-auto"
    >
      <div className="container px-6 py-10 mx-auto border rounded-2xl">
        <div className="mt-8 lg:-mx-6 lg:items-center px-4">
          <div className="grid md:grid-cols-1 lg:grid-cols-1 xl:grid-cols-2 2xl:grid-cols-2 gap-4">
            <div className="px-1">
              <h1 className="px-2 text-xl">Trading:</h1>
              <div className="grid grid-cols-2 overflow-y-auto h-72">
                {post["trading_img"].map((card) => {
                  return (
                    <div className="ml-3 leading-6 space-y-5 py-2 px-2">
                      <img
                        className="w-auto mobile:h-40 sm:h-56 md:h-56 lg:h-56 xl:h-56 2xl:h-56 py-1"
                        src={card}
                        alt=""
                        key={card}
                      />
                    </div>
                  );
                })}
              </div>
            </div>
            <div className="px-1">
              <h1 className="px-2 text-xl">Looking for:</h1>
              <div className="grid grid-cols-2 overflow-y-auto h-72">
                {post["wants_img"].map((card) => {
                  return (
                    <div className="ml-3 leading-6 space-y-5 py-2 px-2">
                      <img
                        className="w-auto mobile:h-40 sm:h-56 md:h-56 lg:h-56 xl:h-56 2xl:h-56 py-1"
                        src={card}
                        alt=""
                        key={card}
                      />
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
          <div
            className="mt-6 lg:w-1/2 lg:mt-0 lg:mx-6"
            style={{ flexBasis: "30%" }}
          >
            <h1 className="text-5xl">{post.title}</h1>

            <p className="mt-3 text-sm text-gray-300 dark:text-gray-300 md:text-sm overflow-auto">
              {post.body}
            </p>

            <div className="flex items-center mt-6">
              <img
                className="object-cover object-center w-10 h-10 rounded-full"
                src={`${process.env.REACT_APP_API_HOST}${post.avatar_pic}`}
                alt=""
              />

              <div className="mx-4">
                <h1 className="text-sm text-white dark:text-gray-200">
                  {post.username}
                </h1>
                {token ? (
                  <p className="text-sm text-gray-300 dark:text-gray-400">
                    Contact me at: {post.contact_info}
                  </p>
                ) : null}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Post;
