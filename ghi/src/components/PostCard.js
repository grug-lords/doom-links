const PostCard = ({ card, checked }) => {
  return (
    <div className="relative px-2 py-2">
      <div
        className={
          checked
            ? "px-1 py-1 border-solid border-white border-2 rounded"
            : "px-1 py-1"
        }
      >
        <input
          key={card.card_id}
          value={card.card_id}
          id={card.card_id}
          type="checkbox"
          className="appearance-none"
        />
        <label htmlFor={card.card_id}>
          <img
            src={card.image}
            alt="Get new eyes"
            className="h-56 hover:cursor-pointer relative top-1"
          />
        </label>
      </div>
    </div>
  );
};

export default PostCard;
