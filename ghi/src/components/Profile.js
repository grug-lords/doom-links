import useToken from "@galvanize-inc/jwtdown-for-react";

export default function Profile({ userdata }) {
  const token = useToken();
  if (token && userdata.username) {
    return (
      <>
        <section className="mt-10 p-4">
          <div className="w-full md:w-1/2 md:mx-auto flex flex-col md:flex-row items-center justify-center text-center">
            <img
              className="inline-flex object-cover border-4 border-gray-500 rounded-full bg-gray-50 h-24 w-24 mb-4 md:mb-0 ml-0 md:mr-5"
              src={`${process.env.REACT_APP_API_HOST}${userdata.avatar_pic}`}
              alt="" 
              onError={(e) =>
                (e.target.src =
                  "https://upload.wikimedia.org/wikipedia/commons/a/ac/Default_pfp.jpg")
              }
            />
            <div className="flex flex-col">
              <div className="md:text-justify mb-3">
                <div className="flex flex-col mb-5">
                  <p className="text-gray-500 font-bold text-xl">
                    {userdata.username}
                  </p>

                  <ul className="mt-2 flex flex-row items-center justify-center md:justify-start ">
                    <li className="mr-5">
                      <p className="text-gray-300 font-bold text-center md:text-left">
                        Email: {userdata.email}
                      </p>
                    </li>
                  </ul>
                </div>

                <p className="text-gray-300 font-semibold md:text-left">
                  Location: {userdata.location}
                </p>
                <p className="text-gray-300 font-semibold md:text-left">
                  Reachme: {userdata.contact_info}
                </p>
              </div>
            </div>
          </div>
        </section>
      </>
    );
  } else {
    return null;
  }
}
