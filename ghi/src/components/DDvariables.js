export const placeholder = [
  {
    id: 1,
    name: "Filter by Color",
    value: "",
    avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTTx5vRzYnJvnsT2tibmS5xQ_mp_S2UuPicCw&usqp=CAU'
  },
  {
    id:2,
    name: "Filter by Type",
    value: '',
    avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTTx5vRzYnJvnsT2tibmS5xQ_mp_S2UuPicCw&usqp=CAU'
  },
  {
    id:3,
    name: "Filter by Points",
    value: '',
    avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTTx5vRzYnJvnsT2tibmS5xQ_mp_S2UuPicCw&usqp=CAU'
  },
  {
    id:4,
    name: "Filter by SubType",
    value: '',
    avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTTx5vRzYnJvnsT2tibmS5xQ_mp_S2UuPicCw&usqp=CAU'
  },
  {
    id:5,
    name: "Filter by Collection",
    value: '',
    avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTTx5vRzYnJvnsT2tibmS5xQ_mp_S2UuPicCw&usqp=CAU'
  },
  {
    id:6,
    name: "Filter by Rarity",
    value: '',
    avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTTx5vRzYnJvnsT2tibmS5xQ_mp_S2UuPicCw&usqp=CAU'
  }
]
export const colors = [
  {
    id: 1,
    name: 'Blue',
    value: "color",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/61b256758f3e5702d0c6ccf3_Blue.png',
  },
  {
    id: 2,
    name: 'Green',
    value: "color",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/61b2567fc92fbb5d6dba0c51_Green.png',
  },
  {
    id: 3,
    name: 'Red',
    value: "color",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/61b256c1f730d7e0e7584025_Red.png',
  },
  {
    id: 4,
    name: 'Purple',
    value: "color",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/61b256b8a9af9bce430928dc_Purple.png',
  },
  {
    id: 5,
    name: 'Colorless',
    value: "color",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/61b25687599d174a931d6521_Colorless.png',
  },
  {
    id: 6,
    name: 'Multi-Color',
    value: "color",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/61b256cb994eb55ee552013a_MultiColor.png',
  },
]

export const types = [
  {
    id: 1,
    name: 'Trait',
    value: "card_type",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/6183f3460f3c016438bd085a_Trait.png',
  },
  {
    id: 2,
    name: 'Age',
    value: "card_type",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/6183f362b3d3d177c4f611d9_Agev2.png',
  },
  {
    id: 3,
    name: 'Sign',
    value: "card_type",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/6183f294dfba0fa4e74a7666_Meaning%20of%20Life.png',
  }
]
export const points = [
  {
    id: 1,
    name: "Zero or Less",
    value: "points",
    points: 0,
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/6183f1a12582de466f3d4f9d__%3D0.png',
  },
  {
    id: 2,
    name: "1",
    value: "points",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/6183f1c7d43cdf2da530454e_1.png',
  },
  {
    id: 3,
    name: "2",
    value: "points",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/6183f1cb421dc4686055938f_2.png',
  },
  {
    id: 4,
    name: "3",
    value: "points",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/6183f1d1cebb897e194c8a9a_3.png',
  },
  {
    id: 5,
    name: "4",
    value: "points",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/6183f1d65119bc6f984469b7_4.png',
  },
  {
    id: 6,
    name: "5",
    value: "points",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/6183f1db4ed5c667c069ac70_5.png',
  },
  {
    id: 7,
    name: "6 or More",
    points: 6,
    value: "points",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/6183f1e1f0946ef6bd704eeb_6%2B.png',
  },
  {
    id: 8,
    name: "variable",
    value: "points",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/61b25738d9af8e692dfbd7b4_variable%20v2.png',
  },

]

export const subtypes = [
  {
    id: 1,
    name: "Dominant",
    value: "sub_type",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/61818eafd135961e6728d800_Dominant.png',
  },
  {
    id: 2,
    name: "Action",
    value: "sub_type",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/61818ed121b7937b0ef05547_Action.png',
  },
  {
    id: 3,
    name: "Play When",
    value: "sub_type",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/6183f3a3412e8779cbf8a325_Play%20When.png',
  },
  {
    id: 4,
    name: "Drop of Life",
    value: "sub_type",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/6183f38e2522cd50f0d24eb7_Drop%20of%20Life.png',
  },
  {
    id: 5,
    name: "Persistent",
    value: "sub_type",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/6183f41373942c48cf0aa49c_Persistent.png',
  },
  {
    id: 6,
    name: "Gene Pool",
    value: "sub_type",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/61b8bd99d483434d16158af8_Effectless.png',
  },
  {
    id: 7,
    name: "Catastrophe",
    value: "sub_type",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/61b255923434c16fbd9dc882_Catastrophe.png',
  },
  {
    id: 8,
    name: "End of World",
    value: "sub_type",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/6183f39ab4d7e64f6c4600a8_World_s%20End.png',
  },
  {
    id: 9,
    name: "Effectless",
    value: "sub_type",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/61b2556fc444618fa88f6ac6_Gene%20Pool.png',
  },
  {
    id: 10,
    name: "Requirement",
    value: "sub_type",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/62191c4b6886280b0ca963dc_Requirement_unpressed.png',
  },
  {
    id: 11,
    name: "Attachment",
    value: "sub_type",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/62191b86ea23102623b1c1da_Artboard%202.png',
  },
]
export const collections = [
  {
    id: 1,
    name: "Classic",
    value: "collection",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/6183f20ce58410e94fe4503c_Classic.png',
  },
  {
    id: 2,
    name: "Special Edition",
    value: "collection",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/6183f2a75119bc0605446da1_KSE.png',
  },
  {
    id: 3,
    name: "Multi-Color",
    value: "collection",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/61b2528d98247a42f9204510_MultiColorv3.png',
  },
  {
    id: 4,
    name: "Dinolings",
    value: "collection",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/6183f21fb4d7e6d9e645faca_Dinolings.png',
  },
  {
    id: 5,
    name: "Mythlings",
    value: "collection",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/6183f229cebb892ef54c8ebb_Mythlings.png',
  },
  {
    id: 6,
    name: "Techlings",
    value: "collection",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/61b25261a8436915189206d4_Techlings.png',
  },
  {
    id: 7,
    name: "Meaning of Life",
    value: "collection",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/6183f294dfba0fa4e74a7666_Meaning%20of%20Life.png',
  },
  {
    id: 8,
    name: "Overlush",
    value: "collection",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/642595ea6c3eece1a2add4f1_Overlush%20Compendium%20Icon.png',
  },
]
export const rarity = [
  {
    id: 1,
    name: "Bundle",
    value: "rarity",
    avatar:
      '',
  },
  {
    id: 2,
    name: "Common",
    value: "rarity",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/63ea9f0341f19ebd17067d93_Common.png',
  },
  {
    id: 3,
    name: "Unusual",
    value: "rarity",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/63ea9f705f3cc65888f3b5be_Unusual.png',
  },
  {
    id: 4,
    name: "Scarce",
    value: "rarity",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/63eac1804a9310103c937d70_Scarce.png',
  },
  {
    id: 5,
    name: "Endangered",
    value: "rarity",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/63ea9f81919f872ab2cfbabc_Endangered.png',
  },
  {
    id: 6,
    name: "Legendary",
    value: "rarity",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/63ea9f8dc0cafbbfeaba3574_Legendary.png',
  },
  {
    id: 7,
    name: "Relic",
    value: "rarity",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/6495ab00411f1c7942673f55_Relic-Icon.png',
  },
  {
    id: 8,
    name: "Exclusive",
    value: "rarity",
    avatar:
      'https://global-uploads.webflow.com/61494314e1322b4a29b5462a/63ea9f8dc0cafbbfeaba3574_Legendary.png',
  },


]
