/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      screens: {
        mobile: "360px",
      },
      width: {
        "md-scr": "496px",
        "width-2xl": "23%",
        "width-xl": "18%",
        "width-lg": "17%",
        "right-img": "16%",
        "right-img-lg": "20%",
      },
      height: {
        "md-img": "5%",
      },
      position: { top: { "19%": "19%" } },
    },
  },
  plugins: [],
};
