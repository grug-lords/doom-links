from fastapi import FastAPI  # , HTTPException
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
import os
from routers import card, posts, accounts, collection
from authenticator import authenticator

app = FastAPI()
app.mount("/static", StaticFiles(directory="app/static"), name="static")
app.include_router(card.router, tags=["cards"])
app.include_router(collection.router, tags=["collection"])
app.include_router(posts.router, tags=["posts"])
app.include_router(accounts.router, tags=["accounts"])
app.include_router(authenticator.router, tags=["accounts"])


app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST", "http://localhost:3000")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00",
        }
    }


# pipelines
@app.get("/")
def root():
    return {"message": "You hit the root path!"}
