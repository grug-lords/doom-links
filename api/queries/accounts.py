from pydantic import BaseModel
from queries.pool import pool
from typing import List, Union, Optional
from fastapi import UploadFile
from PIL import Image
import secrets


class UserIn(BaseModel):
    username: str
    email: str
    password: str
    password_confirmation: str


class UserOut(BaseModel):
    user_id: int
    username: str
    email: str


class UserUpdateIn(BaseModel):
    location: Optional[str] = None
    contact_info: Optional[str] = None


class UserProfileOut(BaseModel):
    user_id: int
    username: str
    email: str
    avatar_pic: Optional[str] = None
    location: Optional[str] = None
    contact_info: Optional[str] = None


class UserOutWithPassword(UserOut):
    hashed_password: str


class DuplicateAccountError(ValueError):
    pass


class Error(BaseModel):
    message: str


class AccountsRepository(BaseModel):
    def create_account(self, users: UserIn, hashed_password: str) -> UserOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO users
                            (username, email, hashed_password)
                        VALUES
                            (%s,%s,%s)
                        RETURNING user_id;
                        """,
                        [
                            users.username.lower(),
                            users.email,
                            hashed_password,
                        ],
                    )
                    user_id = result.fetchone()[0]
                    return UserOutWithPassword(
                        user_id=user_id,
                        username=users.username,
                        email=users.email,
                        hashed_password=hashed_password,
                    )
        except Exception as e:
            print(e)
            raise DuplicateAccountError from e

    def get_account(self, username: str) -> UserOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        user_id,
                        username,
                        email,
                        hashed_password,
                        location,
                        contact_info,
                        avatar_pic
                        FROM users
                        WHERE username = %s
                        """,
                        [username],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return UserOutWithPassword(
                        user_id=record[0],
                        username=record[1],
                        email=record[2],
                        hashed_password=record[3],
                    )
        except Exception as e:
            print(e)
            return {"message": "Account not found"}

    def view_account(self, username: str) -> UserProfileOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        user_id,
                        username,
                        email,
                        location,
                        contact_info,
                        avatar_pic
                        FROM users
                        WHERE username = %s
                        """,
                        [username],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return UserProfileOut(
                        user_id=record[0],
                        username=record[1],
                        email=record[2],
                        location=record[3],
                        contact_info=record[4],
                        avatar_pic=record[5],
                    )
        except Exception as e:
            print(e)
            return {"message": "Account not found"}

    def get_all_users(self) -> Union[List[UserOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT user_id, username, email
                        FROM users
                        """
                    )
                    return [
                        self.record_to_user_out(record) for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all users"}

    async def update_avatar(
            self,
            user_id: int,
            file: UploadFile,
    ) -> Union[None, Error]:

        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    FILEPATH = "./app/static/images/"
                    filename = file.filename
                    extension = filename.split(".")[1]

                    if extension not in ["png", "jpg", "jpeg"]:
                        return {
                            "status": "error",
                            "message": "Invalid file extension"
                            }

                    token_name = secrets.token_hex(10) + "." + extension
                    generated_name = FILEPATH + token_name
                    file_content = await file.read()
                    with open(generated_name, "wb") as file:
                        file.write(file_content)

                    img = Image.open(generated_name)
                    MAX_SIZE = (200, 200)
                    img.thumbnail(MAX_SIZE)
                    img.save(generated_name)
                    generated_name_split = generated_name.split("./app")
                    pic_url = generated_name_split[1]

                    result = db.execute(
                        """
                        UPDATE users
                        SET avatar_pic = %s
                        WHERE user_id = %s
                        RETURNING user_id;
                        """,
                        [pic_url, user_id],
                    )
                    return result.fetchone()[0]

        except Exception as e:
            print("e is: " + str(e))
            return {"message": "Could not update account"}

    def update_account(
        self,
        user_id: int,
        update_in: UserUpdateIn,
    ) -> Union[None, Error]:

        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    set_clause = []
                    if update_in.location:
                        set_clause.append(f"location = '{update_in.location}'")
                    if update_in.contact_info:
                        set_clause.append(
                            f"contact_info = '{update_in.contact_info}'"
                        )

                    update = f"""
                            UPDATE users
                            SET {', '.join(set_clause)}
                            WHERE user_id = %s
                            RETURNING user_id;
                            """

                    result = db.execute(
                        update,
                        [
                            user_id,
                        ],
                    )
                    return result.fetchone()[0]

        except Exception as e:
            print(e)
            return {"message": "Could not update account"}

    def record_to_user_out(self, record):
        return UserOut(
            user_id=record[0],
            username=record[1],
            email=record[2],
        )

    def delete_account(self, user_id: int) -> Union[None, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        DELETE FROM users
                        WHERE user_id = %s
                        RETURNING user_id, username, email;
                        """,
                        [user_id],
                    )
                    return self.record_to_user_out(result.fetchone())

        except Exception as e:
            print(e)
            return {"message": "Could not delete account"}
