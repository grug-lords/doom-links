from pydantic import BaseModel
from fastapi import HTTPException
from datetime import datetime
from typing import List, Optional
from queries.pool import pool


class PostIn(BaseModel):
    user_id: int
    trading: list[int]
    wants: list[int]
    title: str
    body: str


class PostOut(BaseModel):
    id: int
    user_id: int
    trading: list[int]
    wants: list[int]
    title: str
    body: str
    status: str
    created: datetime
    username: str
    contact_info: Optional[str] = None
    avatar_pic: Optional[str] = None


class PostOutCreate(BaseModel):
    id: int
    user_id: int
    trading: list[int]
    wants: list[int]
    title: str
    body: str
    status: str
    created: datetime


class PostUpdateIn(BaseModel):
    trading: list[int]
    wants: list[int]
    title: str
    body: str


class PostsRepository:
    def get_all(self) -> List[PostOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                        p.id,
                        p.user_id,
                        p.trading,
                        p.wants,
                        p.title,
                        p.body,
                        p.status,
                        p.created,
                        u.username,
                        u.contact_info,
                        u.avatar_pic
                        FROM posts as p
                        INNER JOIN users as u
                        ON p.user_id = u.user_id
                        ORDER BY created DESC
                        """
                    )
                    result = []
                    for record in db:
                        post = PostOut(
                            id=record[0],
                            user_id=record[1],
                            trading=record[2],
                            wants=record[3],
                            title=record[4],
                            body=record[5],
                            status=record[6],
                            created=record[7],
                            username=record[8],
                            contact_info=record[9],
                            avatar_pic=record[10],
                        )
                        result.append(post)
                    return result
        except Exception:
            raise HTTPException(status_code=400, detail="Could not get posts")

    def get_one(self, post_id: int) -> PostOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                        p.id,
                        p.user_id,
                        p.trading,
                        p.wants,
                        p.title,
                        p.body,
                        p.status,
                        p.created,
                        u.username,
                        u.contact_info,
                        u.avatar_pic
                        FROM posts as p
                        INNER JOIN users as u
                        ON p.user_id = u.user_id
                        WHERE id = %s;
                        """,
                        [post_id],
                    )

                    record = db.fetchone()
                    return PostOut(
                        id=record[0],
                        user_id=record[1],
                        trading=record[2],
                        wants=record[3],
                        title=record[4],
                        body=record[5],
                        status=record[6],
                        created=record[7],
                        username=record[8],
                        contact_info=record[9],
                        avatar_pic=record[10],
                    )

        except Exception:
            raise HTTPException(status_code=404, detail="Not Found")

    def create(self, post: PostIn) -> PostOutCreate:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    status = "created"
                    curr_time = datetime.now()
                    db.execute(
                        """
                        INSERT INTO posts
                            (user_id,
                            trading,
                            wants,
                            title,
                            body,
                            status,
                            created)
                        VALUES
                            (%s, %s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            post.user_id,
                            post.trading,
                            post.wants,
                            post.title,
                            post.body,
                            status,
                            curr_time,
                        ],
                    )
                    id = db.fetchone()[0]
                    return PostOutCreate(
                        id=id,
                        user_id=post.user_id,
                        trading=post.trading,
                        wants=post.wants,
                        title=post.title,
                        body=post.body,
                        status=status,
                        created=curr_time,
                    )
        except Exception:
            raise HTTPException(
                status_code=400, detail="Could not create post"
            )

    def update(self, post_id: int, post: PostUpdateIn) -> PostOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE posts
                        SET trading = %s,
                            wants = %s,
                            title = %s,
                            body = %s
                        WHERE id = %s
                        RETURNING id,
                            user_id,
                            trading,
                            wants,
                            title,
                            body,
                            status,
                            created;
                        """,
                        [
                            post.trading,
                            post.wants,
                            post.title,
                            post.body,
                            post_id,
                        ],
                    )
                    record = db.fetchone()
                    return PostOut(
                        id=record[0],
                        user_id=record[1],
                        trading=record[2],
                        wants=record[3],
                        title=record[4],
                        body=record[5],
                        status=record[6],
                        created=record[7],
                    )
        except Exception:
            raise HTTPException(
                status_code=400, detail="Could not update post"
            )

    def delete(self, post_id) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM posts
                    WHERE id = %s
                    RETURNING id;
                    """,
                    [post_id],
                )
                try:
                    db.fetchone()[0]
                except TypeError:
                    raise HTTPException(
                        status_code=404,
                        detail=f"Post at id:{post_id} not found",
                    )
                return True
