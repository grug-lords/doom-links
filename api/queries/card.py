# import datetime
from queries.pool import pool
from pydantic import BaseModel
from typing import Optional, List
from .accounts import Error


class CardOut(BaseModel):
    card_id: float
    card_name: str
    color: str
    card_type: str
    sub_type: Optional[str]
    points: Optional[str]
    collection: str
    sub_collection: Optional[str]
    rarity: str
    image: str


class DoomlingsCardRepository:
    def get_card(self, card_id: float) -> Optional[CardOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM cards
                        WHERE id = %s
                        """,
                        [card_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return {
                            "message": "Could not find a card with that ID"
                        }
                    return self.record_to_card_out(record)

        except Exception as e:
            print(e)
            return {"message": "Could not find card"}

    def get_all_cards(self) -> List[CardOut] | Error:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM cards
                        ORDER BY id;
                        """
                    )
                    return [self.record_to_card_out(record) for record in db]

        except Exception as e:
            print(e)
            return {"message": "Could not find all cards"}

    def record_to_card_out(self, record):
        return CardOut(
            card_id=record[0],
            card_name=record[1],
            color=record[2],
            card_type=record[3],
            sub_type=record[4],
            points=record[5],
            collection=record[6],
            sub_collection=record[7],
            rarity=record[8],
            image=record[9],
        )
