from pydantic import BaseModel
from fastapi import HTTPException
from typing import Optional, List
from queries.pool import pool


class CollectionGetImageOut(BaseModel):
    id: int
    user_id: int
    card_id: float
    trading: bool
    amount: int
    holos: int
    image: str
    name: str


class IdOut(BaseModel):
    card_id: float


class CollectionOut(BaseModel):
    id: int
    user_id: int
    card_id: float
    trading: bool
    amount: int
    holos: int


class CollectionIn(BaseModel):
    card_id: float
    trading: Optional[bool]
    amount: int
    holos: int


class CollectionRepository:

    def add_to_collection(
        self, collection: CollectionIn, user_id
    ) -> CollectionOut:
        if not collection.trading:
            collection.trading = False
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO collection (
                        user_id,
                        card_id,
                        trading,
                        amount,
                        holos
                        )
                        VALUES
                            (%s, %s, %s, %s, %s)
                        RETURNING *;
                        """,
                        [
                            user_id,
                            collection.card_id,
                            collection.trading,
                            collection.amount,
                            collection.holos,
                        ],
                    )
                    record = result.fetchone()
                    return self.record_to_collection_out(record)

        except Exception:
            raise HTTPException(
                status_code=400, detail="Could not add card to user collection"
            )

    def record_to_collection_out(self, record):
        return CollectionOut(
            id=record[0],
            user_id=record[1],
            card_id=record[2],
            trading=record[3],
            amount=record[4],
            holos=record[5],
        )

    def get_user_collection(self, user_id: int) -> List[CollectionGetImageOut]:

        try:
            with pool.connection() as conn:
                with conn.cursor() as db:

                    db.execute(
                        """
                        SELECT c.id, c.user_id, c.card_id, c.trading, c.amount,
                        c.holos, d.image, d.name
                        FROM collection as c
                        INNER JOIN cards as d
                        ON c.card_id = d.id
                        WHERE user_id = %s;
                        """,
                        [user_id],
                    )
                    result = []
                    for record in db:
                        card = CollectionGetImageOut(
                            id=record[0],
                            user_id=record[1],
                            card_id=record[2],
                            trading=record[3],
                            amount=record[4],
                            holos=record[5],
                            image=record[6],
                            name=record[7],
                        )
                        result.append(card)
                    return result
        except Exception:
            raise HTTPException(
                status_code=400, detail="Could not retrieve user collection"
            )

    def get_id_from_collection(self, card_id: float, user_id: int) -> IdOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT card_id
                        FROM collection
                        WHERE user_id = %s AND card_id = %s
                        """,
                        [user_id, card_id],
                    )
                    record = result.fetchone()[0]
                    return IdOut(card_id=record)
        except Exception:
            return IdOut(card_id=0)
