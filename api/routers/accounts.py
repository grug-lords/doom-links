from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
    UploadFile,
)
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator

from pydantic import BaseModel

from queries.accounts import (
    UserIn,
    UserOut,
    UserUpdateIn,
    AccountsRepository,
    DuplicateAccountError,
    Error,
    UserProfileOut,
)

from typing import Union, List


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: UserOut


class UserAccount(BaseModel):
    user_id: int


class UsernameOut(BaseModel):
    username: str


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.post("/accounts", response_model=AccountToken | HttpError)
async def create_account(
    info: UserIn,
    request: Request,
    response: Response,
    repo: AccountsRepository = Depends(),
):
    if info.password != info.password_confirmation:
        raise HTTPException(
            status_code=400,
            detail="Passwords do not match",
        )
    hashed_password = authenticator.hash_password(info.password)
    try:
        account = repo.create_account(info, hashed_password)
    except DuplicateAccountError:
        raise HTTPException(
            status_code=400,
            detail="An Account already exists with these credentials",
        )
    form = AccountForm(username=info.username, password=info.password)
    token = await authenticator.login(response, request, form, repo)
    return AccountToken(account=account, **token.dict())


@router.get("/users", response_model=Union[Error, List[UserOut]])
def get_all_users(
    repo: AccountsRepository = Depends(),
):
    return repo.get_all_users()


@router.get("/users/self", response_model=UserProfileOut)
async def view_self(
    repo: AccountsRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    """
    View own profile.
    """
    user = repo.view_account(username=user_data["username"])
    if user_data is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Must be logged in to view user profile",
        )
    return user


@router.get("/users/{username}", response_model=UserProfileOut)
async def view_profile(
    username: str,
    repo: AccountsRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    """
    View user profile.
    """
    user = repo.view_account(username=username)

    if user is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User not found",
        )
    if user_data is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Must be logged in to view user profile",
        )

    return user


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.put("/users/self/profile", response_model=UserAccount | None)
async def update_avatar(
    file: UploadFile,
    repo: AccountsRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        if user_data is not None:
            user_id = user_data["user_id"]
            updated_user_id = await repo.update_avatar(user_id, file)
            return UserAccount(user_id=updated_user_id)
        else:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Must be logged in to update account",
            )
    except Exception as e:
        print(e)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Could not update account",
        )


@router.put("/users/self", response_model=UserAccount | None)
async def update_account(
    update_in: UserUpdateIn,
    repo: AccountsRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        if user_data is not None:
            user_id = user_data["user_id"]
            updated_user_id = repo.update_account(user_id, update_in)
            return UserAccount(user_id=updated_user_id)
        else:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Must be logged in to update account",
            )
    except Exception as e:
        print(e)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Could not update account",
        )


@router.delete("/users/{id}", response_model=UserProfileOut)
async def delete_account(
    id: int,
    repo: AccountsRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        deleted_user = repo.delete_account(id)
        if deleted_user is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="User not found",
            )
        return deleted_user

    except Exception as e:
        print(e)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Could not delete account",
        )
