from fastapi import APIRouter, Depends, HTTPException
from typing import List, Optional
from queries.collection import (
    CollectionIn,
    CollectionOut,
    CollectionRepository,
    CollectionGetImageOut,
    IdOut,
)
from queries.card import Error
from authenticator import authenticator

router = APIRouter()


@router.post("/doomlings/collection/", response_model=CollectionOut)
def add_to_collection(
    collection: CollectionIn,
    repo: CollectionRepository = Depends(),
    user_data: dict = Depends(authenticator.get_current_account_data),
):

    result = repo.add_to_collection(collection, user_data["user_id"])
    return result


@router.get(
    "/doomlings/collection", response_model=List[CollectionGetImageOut]
)
def get_user_collection(
    user_data: dict = Depends(authenticator.get_current_account_data),
    repo: CollectionRepository = Depends(),
):
    """
    Get the collection for the authenticated user.
    """
    if user_data is None:
        raise HTTPException(
            status_code=400,
            detail="Must be logged in to fetch user collection",
        )

    user_id = user_data["user_id"]
    user_collection = repo.get_user_collection(user_id)
    return user_collection


@router.get(
    "/doomlings/collection/{card_id}/",
    response_model=Optional[IdOut] | Error,
)
def get_id_from_collection(
    card_id: float,
    user_data: dict = Depends(authenticator.get_current_account_data),
    repo: CollectionRepository = Depends(),
):
    if user_data is None:
        raise HTTPException(
            status_code=400,
            detail="Must be logged in to fetch user collection",
        )
    user_id = user_data.get("user_id")
    card_id = repo.get_id_from_collection(card_id, user_id)
    return card_id
