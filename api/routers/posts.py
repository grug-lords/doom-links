from fastapi import APIRouter, Depends
from typing import List
from queries.posts import (
    PostOut,
    PostIn,
    PostsRepository,
    PostUpdateIn,
    PostOutCreate,
)


router = APIRouter()


@router.get("/posts", response_model=List[PostOut])
def get_all_posts(repo: PostsRepository = Depends()):
    result = repo.get_all()
    return result


@router.get("/posts/{post_id}", response_model=PostOut)
def get_single_post(post_id: int, repo: PostsRepository = Depends()):
    result = repo.get_one(post_id)
    return result


@router.post("/posts", response_model=PostOutCreate)
def create_post(post: PostIn, repo: PostsRepository = Depends()):
    result = repo.create(post)
    return result


@router.put("/posts/{post_id}", response_model=PostOut)
def update_post(
    post_id: int, post: PostUpdateIn, repo: PostsRepository = Depends()
):
    result = repo.update(post_id, post)
    return result


@router.delete("/posts/{post_id}", response_model=bool)
def delete_post(post_id: int, repo: PostsRepository = Depends()):
    result = repo.delete(post_id)
    return result
