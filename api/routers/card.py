from fastapi import APIRouter, Depends, Response
from typing import List, Optional
from queries.card import CardOut, DoomlingsCardRepository, Error

router = APIRouter()


@router.get("/doomlings/catalog/", response_model=List[CardOut] | Error)
def get_all_cards(
    response: Response,
    repo: DoomlingsCardRepository = Depends(),
):
    model = repo.get_all_cards()
    if type(model) is list:
        return model
    else:
        response.status_code = 400
        return model


@router.get(
    "/doomlings/catalog/{card_id}/", response_model=Optional[CardOut] | Error
)
def get_card(
    card_id: float,
    response: Response,
    repo: DoomlingsCardRepository = Depends(),
):
    card = repo.get_card(card_id)
    if type(card) is dict:
        response.status_code = 400
    return card
