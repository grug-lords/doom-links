steps = [
    [
        """
    ALTER TABLE collection
    RENAME COLUMN owned to card_id;
    """,
        """
    DROP TABLE collection
    """,
    ],
    [
        """
    ALTER TABLE collection
    ALTER COLUMN trading SET DEFAULT false;
    """,
        """
    DROP TABLE collection
    """,
    ],
]
