steps = [
    [
        """
        ALTER TABLE users
        ADD COLUMN is_admin BOOLEAN DEFAULT FALSE,
        ADD COLUMN avatar_pic TEXT,
        DROP COLUMN avatar_image;
        """,
        """
        DROP TABLE users;
        """,
    ],
]
