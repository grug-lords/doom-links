steps = [
    [
        """
    CREATE TABLE cards (
        id FLOAT PRIMARY KEY NOT NULL,
        name TEXT NOT NULL,
        color TEXT NOT NULL,
        type TEXT NOT NULL,
        sub_type TEXT,
        points TEXT,
        collection TEXT NOT NULL,
        sub_collection TEXT,
        rarity TEXT NOT NULL,
        image TEXT NOT NULL
    );
    """,
        """
    DROP TABLE cards;
    """,
    ],
    [
        """
    CREATE TABLE users (
        user_id SERIAL PRIMARY KEY NOT NULL,
        username VARCHAR NOT NULL UNIQUE,
        email VARCHAR NOT NULL UNIQUE,
        hashed_password VARCHAR NOT NULL,
        location VARCHAR,
        contact_info TEXT,
        avatar_image BYTEA
    );
    """,
        """
    DROP TABLE users;
    """,
    ],
    [
        """
        CREATE TABLE posts (
            id SERIAL PRIMARY KEY NOT NULL,
            user_id INT NOT NULL,
            FOREIGN KEY (user_id) REFERENCES users(user_id),
            trading INT[] not NULL,
            wants INT[] NOT NULL,
            title VARCHAR(100) NOT NULL,
            body TEXT NOT NULL,
            status VARCHAR(30) NOT NULL,
            created TIMESTAMP WITH TIME ZONE NOT NULL
        );
        """,
        """
        DROP TABLE posts;
        """,
    ],
]
