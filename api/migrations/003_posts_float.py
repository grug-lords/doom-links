steps = [
    [
        """
        ALTER TABLE posts
        ALTER COLUMN trading TYPE FLOAT[],
        ALTER COLUMN wants TYPE FLOAT[];
        """,
        """
        DROP TABLE posts;
        """,
    ]
]
