steps = [
    [
        """
    CREATE TABLE collection (
    id SERIAL PRIMARY KEY NOT NULL,
    user_id INT NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users,
    owned FLOAT NOT NULL,
    FOREIGN KEY (owned) REFERENCES cards,
    trading BOOL NOT NULL,
    amount INT NOT NULL,
    holos INT NOT NULL
    );
    """,
        """
    DROP TABLE collection
    """,
    ]
]
