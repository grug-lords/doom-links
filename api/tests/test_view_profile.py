from main import app
from fastapi.testclient import TestClient
from queries.accounts import AccountsRepository
from authenticator import authenticator


client = TestClient(app)


def fake_get_current_account_data():
    print("do stuff")
    return {"user_id": 1, "username": "user1", "email": "user1@email.com"}


def fake_get_current_account_data_failed():
    return None


class emptyAccountsRepository:
    def view_account(self, username):
        return {
            "user_id": 1,
            "username": "user1",
            "email": "user1@email.com",
            "location": None,
            "contact_info": None,
            "avatar_pic": None,
        }


def test_view_own_profile():
    app.dependency_overrides[AccountsRepository] = (
        emptyAccountsRepository
    )
    app.dependency_overrides[authenticator.get_current_account_data] = (
        fake_get_current_account_data
    )
    response = client.get("/users/self")
    app.dependency_overrides = {}
    assert response.status_code == 200
    assert response.json() == {
        "user_id": 1,
        "username": "user1",
        "email": "user1@email.com",
        "location": None,
        "contact_info": None,
        "avatar_pic": None,
    }


def test_view_profile_not_logged_in():
    app.dependency_overrides[AccountsRepository] = (
        emptyAccountsRepository
    )
    app.dependency_overrides["authenticator.get_current_account_data"] = (
        fake_get_current_account_data_failed
    )
    response = client.get("/users/self")
    app.dependency_overrides = {}
    assert response.status_code == 401
    assert response.json() == {
        "detail": "Invalid token",
    }
