from main import app
from queries.posts import PostsRepository
from fastapi.testclient import TestClient


client = TestClient(app)


class EmptyPostRepository:
    def get_all(self):
        return []


def test_get_all_posts():
    app.dependency_overrides[PostsRepository] = EmptyPostRepository
    response = client.get("/posts")
    app.dependency_overrides = {}
    assert response.status_code == 200
    assert response.json() == []
