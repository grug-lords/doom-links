from fastapi.testclient import TestClient
from main import app
from queries.accounts import AccountsRepository

client = TestClient(app)


class EmptyAccountsRepository:
    def get_all_users(self):
        return []


def fake_get_all_users():
    return []


def test_get_all_users():
    app.dependency_overrides[AccountsRepository] = EmptyAccountsRepository

    response = client.get("/users")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == []
