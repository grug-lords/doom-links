from main import app
from fastapi.testclient import TestClient
from queries.card import DoomlingsCardRepository

client = TestClient(app)


class EmptyDoomlingsCardRepository:
    def get_all_cards(self):
        return []


def test_get_all_cards():
    app.dependency_overrides[DoomlingsCardRepository] = (
        EmptyDoomlingsCardRepository
    )
    response = client.get("/doomlings/catalog/")
    app.dependency_overrides = {}
    assert response.status_code == 200
    assert response.json() == []


def test_init():
    assert 1 == 1
